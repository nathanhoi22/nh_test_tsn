#!/usr/bin/env python
from time import asctime


def Log(message, file = 'mylog.log', mode = 'a'):
    '''message: message to log
    log: path to log file
    mode: type of write to log file
    if mode = 'a':appends to file
    if mode = 'w':writes new file
    '''
    MSG = '%s: %s' %(str(asctime()),str(message))
    print(MSG)
    try:
        event = open(file, mode)
        event.write(MSG + '\n')
        event.close()
    except Exception as error:
        print ('could not log event: %s' %error)                

