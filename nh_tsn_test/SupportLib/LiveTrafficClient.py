'''
###SETUP FOR SERVER####
1. setup twisted server
#install
yum -y install twisted python
#start
twist web --path <MyServerDir> --port 80

2. stop iptables
service iptables stop

3. install/configure vsftpd server
#install
yum -y install vsftpd
#configure /etc/vsftpd/vsftpd.conf
#verify
local_enable=YES
write_enable=YES
#add
local_root= /root/files/Files
local_root= <MyServerDir>
#Remove root in /etc/vsftpd/ftpusers and /etc/vsftpd/user_list to allow root logon
#set ftp full directory access
setsebool -P allow_ftpd_full_access 1
#start service
sevice vsftpd start




###Client (place to run test)
#need python, and python-pip, curl, tcpdump and python modules paramiko and scp
yum -y install python curl tcpdump
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum install python-pip
pip install paramiko
pip install scp

###Need scripts from SupportLib dir
'''


import os
from subprocess import Popen, PIPE
import threading
from MyLogger import *
from SSHController import *
from time import sleep
#####################
###DefaultSettings###
#####################
#filesList
FilesListC = 'FilesList.txt'

#FTP
MyFTPUser = 'root'
MyFTPPass = 'password'
MyServerDir = '/root/files/Files' #directory on server where files are
MyServer = '192.168.1.4'
#local
MyLocalFilesDir = 'LOCALDIR'  #files recieved will be placed here, this is the directory used for put/append

#######################
###ENDDefaultSettins###
#######################


import sys,getopt


Methods = [ 
           'HTTP_GET',
           'HTTP_POST', 
           'HTTP_PUT',
           'FTP_GET',
           'FTP_PUT', #run last
           'FTP_APPEND', #run last
           ] 
HTTPVERSIONS = ['1.0',
                '1.1',
                #'2.0', not working need libcurl 7.33
                ]
COMPRESSIONS = [None,
                #'gzip',
                ]

class  LiveTrafficClient():
    def __init__(self):
        self.cont = 0
        self.TestFiles = []
        ##setting to default settings
        self.SetServerInfo()
        self.SetFilesListClient()
        self.SetServerDir()
        self.SetLocalFilesDir()
        self.portOverride = None
        self.TestCases = []

    def SetServerInfo(self,
                      ftpUser = MyFTPUser,
                      ftpPass = MyFTPPass,
                      ftpServer = MyServer,
                      ):
        self.MyFTPUser = ftpUser
        self.MyFTPPass = ftpPass
        self.MyServer = ftpServer
        
    def SetFilesListClient(self,
                           filesList = FilesListC):
        if os.path.exists(filesList) == False:
            Log('no files in list')
        else:
            for line in open(filesList, 'r').readlines():
                self.TestFiles.append(line.strip('\n').strip(' '))
            return self.TestFiles
    def SetServerDir(self,
                     ServerDir = MyServerDir,
                     ):
        self.MyServerDir = ServerDir
        return self.MyServerDir
    def SetLocalFilesDir(self,
                         LocalFilesDir = MyLocalFilesDir,
                         ):
        self.MyLocalFilesDir = LocalFilesDir
        return self.MyLocalFilesDir
    def SetPortOverRide(self,
                        port):
        self.portOverride = port
    #toRunthis you need to have twistd server already started
    #GET command must be run before put command unless nothing to put
    #@classmethod
    def runCurl(self,
                fileName = '',
                Method  = 'HTTP_GET',
                Client = 'local',
                Server = 'server',
                HttpVer = '1.1',
                Encoding = None,
                ):
        assert fileName != '', 'File path is not defined'
        nRet = 0

        #certain combinations cant be done
        skiprun = 0
        #skip altering the http version on ftp method
        if (HttpVer != '1.1') and ('ftp' in Method.lower()):
            skiprun = 1
        #skip altering compression on ftp method
        if (Encoding != None) and ('ftp' in Method.lower()):
            skiprun = 1
        if skiprun == 1:
            Log('Will not run this curl becausee invalid arg')
        else:   
            ###START BY BUILDING THE COMMAND
            MyCommand = ['curl'] #start command
            PROTO = Method.split('_')[0].lower()
            Method = Method.split('_')[1]
            assert (PROTO == 'http') or (PROTO == 'ftp'), 'protocal %s is not correct.  Needs to be ftp or http' %PROTO
            ##set my server ip from Server
            self.parse = MyXMLParser(Server)
            self.MyServer,self.MyFTPUser,self.MyFTPPass = self.parse.SSHClients[Server]
            
            #define request method first
            MyCommand.append('--request %s' %Method)
            #check if put or get
            if Method.lower() == 'put' or Method.lower() == 'post':
                RunPut = 1
            else:
                RunPut = 0

            #Define Port
            if PROTO == 'http':
                port = 80
            else:
                port = 21
            
            

            #define HTTP Version 
            if PROTO == 'http':
                if HttpVer == '1.0':  #SUPPORT FOR 2.0 LATER
                    MyCommand.append('-0')


            #define put information
            if RunPut == 1:
                if PROTO == 'http':
                    MyCommand.append('-i')
                    MyCommand.append('--form "file=@%s/%s;filename=put_%s" --form file=upload' %(self.MyLocalFilesDir, os.path.basename(fileName), os.path.basename(fileName)))
                    #MyCommand.append('--form "file=@%s/%s;filename=put_%s"'%(self.MyLocalFilesDir.strip('\n'), os.path.basename(fileName).strip('\n'), os.path.basename(fileName).strip('\n')))
                    MyCommand.append('--form file=upload')
                else:
                    if Method.lower() == 'append':
                        MyCommand.append('--append')
                    if Encoding == None:
                        MyCommand.append('-T %s/%s' %(self.MyLocalFilesDir, os.path.basename(fileName)))



            #define encoding
            if PROTO == 'http':
                if Encoding != None:
                    MyCommand.append("--header 'accept-encoding:%s'" %Encoding)
            
            #Now define URL
            if self.portOverride != None:
                port = self.portOverride
            if PROTO == 'http':
                if RunPut == 1:
                    MyCommand.append('%s://%s:%s'%(PROTO,self.MyServer,port))
                else:
                    MyCommand.append('%s://%s:%s/%s'%(PROTO,self.MyServer,port,fileName))
            if PROTO == 'ftp':
                #define user/password
                MyCommand.append('--user %s:%s' %(self.MyFTPUser,self.MyFTPPass))
                #define URL
                if RunPut == 1:
                    MyCommand.append('%s://%s/put_%s'%(PROTO,self.MyServer,os.path.basename(fileName)))
                else:
                    MyCommand.append('%s://%s/%s'%(PROTO,self.MyServer,fileName))
      
            #define get output          
            if RunPut == 0:
                if PROTO == 'http':
                    if Encoding != None:
                        MyCommand.append(' > %s/%s.%s' %(self.MyLocalFilesDir, os.path.basename(fileName),Encoding))
                    else:
                        MyCommand.append(' > %s/%s' %(self.MyLocalFilesDir, os.path.basename(fileName)))
                elif PROTO == 'ftp':
                    if Encoding == None:
                        MyCommand.append(' > %s/%s' %(self.MyLocalFilesDir, os.path.basename(fileName))) #save output to filename
                
            CommandString = ''
            for Command in MyCommand:
                CommandString += Command.strip('\n') + ' '
            Log('running %s' %CommandString)
            #return CommandString
            ################COMMAND BUILT#################################


            #################NOW RUN THE COMMAND############################
            self.sshcontrol = SSHController() ## get sshcontroller
            #connect to server machine
            self.sshcontrol.connect_to_machine(machine = Server)
            if Client == 'local':
                try:
                    Log('running %s' %CommandString)
                    s = Popen(CommandString, executable = '/bin/bash', stdout=PIPE, shell = True)
                except Exception as error:
                    Log('error : %s' %error)
                    nRet = 1
                for Line in s.stdout.readlines():
                            Log(Line)
                assert nRet == 0, 'failed to connect to client machine'
            else:
                #also need to connect to client machine
                try:
                    Log('connecting to %s machine' %Client)
                    self.sshcontrol = SSHController()
                    self.sshcontrol.connect_to_machine(machine = Client)
                    sh_stdin, ssh_stdout, sshstderr = self.sshcontrol.ConnectObjects[Client].sendcommand(CommandString)
                except Exception as error:
                    Log('error : %s' %error)
                    nRet = 1
                for Line in ssh_stdout:
                    Log(Line)
                assert nRet == 0, 'failed to connect to server machine'
            #########################COMMAND RUN################################

            #################verify files match##################################
            Log('verifying files match')
            #Connect again to server
            self.sshcontrol.connect_to_machine(machine = Server)
            if RunPut == 1:
                if self.sshcontrol.RunFileValidation(File1 = ['%s/%s' %(self.MyLocalFilesDir, os.path.basename(fileName)), Client],
                                                     File2 = ['%s/put_%s' %(self.MyServerDir,os.path.basename(fileName)), Server],
                                                     ) != 0:
                    Log('file not match %s:%s -- $s%s' %('local', fileName, self.MyServerDir,fileName))
                    nRet = 2
                assert nRet == 0, 'File Validation failed'
                #Now remove the put files
                self.sshcontrol.ConnectObjects[Server].sendcommand('rm -rf %s/put_%s' %(self.MyServerDir,os.path.basename(fileName)))
                
            else:
                if self.sshcontrol.RunFileValidation(File1 = ['%s/%s' %(self.MyLocalFilesDir, os.path.basename(fileName)), Client],
                                                     File2 = ['%s/%s' %(self.MyServerDir,fileName), Server],
                                                     ) != 0:
                    Log('file not match %s:%s -- $s%s' %('local', fileName, self.MyServerDir,fileName))
                    nRet = 2
                    assert nRet == 0, 'File Validation failed'
                else:
                    Log('file successfully copied')
            assert nRet == 0 , 'files dont match'
    def Generate_Test_Cases(self,
                            TestFiles = [],
                            Methods = [],
                            HTTPVERSIONS = [],
                            ):
        assert TestFiles != [], 'No files Defined'
        assert Methods != [],  'No method Defined'
        assert HTTPVERSIONS != [], 'No http version Defined'
        for METHOD in Methods:
            for fileName in TestFiles:
                for HTTPVERSION in HTTPVERSIONS:
                    skiprun = 0
                    putRun = 0
                    if (HTTPVERSION != '1.1') and ('ftp' in METHOD.lower()):
                        skiprun = 1
                    if METHOD == 'FTP_PUT' or METHOD == 'FTP_APPEND':
                        putRun = 1
                    if skiprun == 0:
                        appendto = [fileName.encode('latin -1', 'ignore'), METHOD.encode('latin -1', 'ignore'),HTTPVERSION.encode('latin -1', 'ignore')]
                        self.TestCases.append(appendto)
        Log('number of tests to run = %i' %len(self.TestCases))
        return self.TestCases
    def RunTest(self,
                CLIENT = '',
                SERVER = '',
                TestCase = [], #format ==>[fileName, METHOD,HTTPVERSION]
                ):
        assert CLIENT != '', 'No Client Defined'
        assert SERVER != '',  'No Server Defined'
        f, METHOD,HTTPVERSION = TestCase
        print 'TestCase', TestCase
        print 'filename', f
        print 'method' , METHOD
        print 'HTTPVERSION', HTTPVERSION
        assert f != '', 'No files Defined'
        assert METHOD != '',  'No method Defined'
        assert HTTPVERSION != '', 'No http version Defined'
        self.runCurl(fileName = f,
                     Method =str(METHOD),
                     Client = str(CLIENT),
                     Server = str(SERVER),
                     HttpVer = HTTPVERSION,
                     )                  
    
        
    @classmethod   
    def cleanup(self,
                CLIENT = 'local',
                ):
        nRet = 0
        #now cleanup
        try:
            Log('running rm -rf %s/*' %self.MyLocalFilesDir)
            if Client == 'local':
                s = Popen('rm -rf %s/*' %self.MyLocalFilesDir, executable = '/bin/bash', stdout=PIPE, shell = True)
            else:
                self.sshcontrol.ConnectObjects[Client].sendcommand('rm -rf %s' %self.MyLocalFilesDir)
        except Exception as error:
            Log('error : %s' %error)
            nRet = 1
        for Line in s.stdout.readlines():
            Log(Line)
        assert nRet ==0 , 'failed to cleanup'

def main(argv):
    Method = ''
    try:
        opts, args = getopt.getopt(argv, 'm:c:s:f:',['method=','client=','server=','path='])
    except getopt.GetoptError:
        print 'failed to run error -m <method>'
    p = 0
    m = 0
    method = 'HTTP_GET'
    client = 'local'
    server = 'server'
    filepath = ''
    for opt, arg in opts:
        if opt == '-m':
            print 'running curl with method %s' %arg
            method = arg.strip('\n')
        if opt == '-c':
            print 'running curl with client %s' %arg
            client = arg.strip('\n')
        if opt == '-s':
            print 'running curl with server %s' %arg
            server = arg.strip('\n')
        if opt == '-f':
            print 'running curl with file %s' %arg
            filepath = arg.strip('\n')
    #now run curl
    self = LiveTrafficClient()
    self.runCurl(fileName = filepath,
                  Method =str(method),
                  Client = str(client),
                  Server = str(server),
                  )
    

def test():
    self = LiveTrafficClient()
    self.runCurl(fileName =self.TestFiles[0] ,
                 Method ='HTTP_GET' ,
                 Client = 'client',
                 Server = 'server',
                 )
    self.runCurl(fileName =self.TestFiles[0] ,
                 Method ='HTTP_PUT' ,
                 Client = 'client',
                 Server = 'server',
                 )
    return self

#if __name__=="__main__":
#    main(sys.argv[1:])

    
