'''
Istall instructions for 

###for tcpreplay, tcprewrite, tcpprep
yum groupinstall "Development Tools"
yum install libpcap-devel
wget http://downloads.sourceforge.net/project/tcpreplay/tcpreplay/3.4.4/tcpreplay-3.4.4.tar.gz
tar xvfvz tcpreplay-3.4.4.tar.gz
cd tcpreplay-3.4.4
./configure --enable-dynamic-link
make
make install
###

###for tcpdump
$ yum -y install tcpdump
###

###for editcap
$ yum -y install wireshark
###

'''
import os
from subprocess import Popen, PIPE
from time import sleep
class TCPHandler():
    def __init__(self):
        self.cont = 0
    def TCPDump(self,
                Args = [], #must be array
                ports = [21, 80],
                eth = 'eth0',
                saveas = 'mypcap.pcap',
                ):
        nRet = 0
        
        #build command
        TCPDumpCommand = []
        TCPStartCommand = 'tcpdump -s 0'
        addportsCommand = 'port '
        for i in len(ports):
            if i == len(ports -1):
                addportsCommand += '%s ' %str(ports[i])
            else:
                addportsCommand += '%s or' %str(ports[i])
        ethCommand = '-i %s' %eth
        saveCommand = '-w %s' %saveas

        TCPDumpCommand.append(TCPStartCommand)
        TCPDumpCommand.append(adportsCommand)
        for Arg in Args:
            TCPDumpCommand.append(Arg)
        TCPDumpCommand.append(ethComand)
        TCPDumpCommand.append(saveCommand)
        myCommandString = ''
        for Command in TCPDumpCommand:
            myCommandString += Command
        print 'will now run command: %s' %myCommandString
        
        #run command
        try:
            self.MyTCPDump = Popen(TCPDumpCommand)
            
        except Exception as Error:
            raise Exception  
        
    def TCPReplay(self,
                  Args = [], #must be array
                  TopSpeed = False,
                  IntF = ['eth0'], #Array of interfaces
                  PcapFile = '',
                  ):
        #build command
        TCPReplayCommand = ['tcpreplay']
        if TopSpeed == True:
            TCPReplayCommand.append('-t')
        for i in range(len(IntF)):
            TCPIntF = '--intf%d=%s' %(i+1,IntF[i])
            TCPReplayCommand.append(TCPIntF)
        for Arg in Args:
            TCPReplayCommand.append(Arg)
        TCPReplayCommand.append(PcapFile)

        myCommandString = ''
        for Command in TCPReplayCommand:
            myCommandString += Command + ' '
        print 'will now run command: %s' %myCommandString
        
        #run command
        try:
            self.MyTCPReplay = Popen(TCPReplayCommand, stdout = PIPE)
            output = []
            for line in self.MyTCPReplay.stdout.readlines():
                output.append(line)
        except Exception as Error:
            raise Exception  
        return output
    def EditCap(self,
                InFile = 'mypcap.pcap',
                OutFile = 'mypcaperror5.pcap',
                BeginArgs = ['-e 0.005', #5% error
                        ],#must be array
                EndArgs = [], #must be array arguments to go at end 
                
                ):
        #build command
        EditCapCommand = ['editcap']
        for Arg in BeginArgs:
            EditCapCommand.append(Arg)
        EditCapCommand.append(InFile)
        EditCapCommand.append(OutFile)
        for Arg in EndArgs :
            EditCapCommand.append(Arg)

        myCommandString = ''
        for Command in EditCapCommand:
            myCommandString += Command + ' '
        print 'will now run command: %s' %myCommandString
        #run command
        try:
            self.EditCapCommand = Popen(EditCapCommand)
            sleep(1)
        except Exception as Error:
            raise Exception  
    def TCPRewrite(self,
                   InFile = '',
                   OutFile = '',
                   CacheFile = None,
                   Args =[],#['--endpoints=192.168.1.5:192.168.1.4'], #must be array
                   ):
        #build command
        if os.path.exists(InFile) == False:
            raise 'the InFile is not there'
        TCPRewriteCommand = ['tcprewrite']
        for Arg in Args:
            TCPRewriteCommand.append(Arg)
        
        if CacheFile != None:
            TCPRewriteCommand.append('--cachefile=%s'%CacheFile)
        TCPRewriteCommand.append('--infile=%s'%InFile)
        TCPRewriteCommand.append('--outfile=%s'%OutFile)

        myCommandString = ''
        for Command in TCPRewriteCommand:
            myCommandString += Command + ' '
        print 'will now run command: %s' %myCommandString
        
        #run command
        try:
            self.TCPRewriteCommand = Popen(TCPRewriteCommand)
            #self.TCPRewriteCommand = Popen(myCommandString)
            sleep(1)
        except Exception as Error:
            raise Exception
            #raise Error
        
    def TCPPrep(self,
                Args = ['--port'],
                CacheFile = '',
                PcapFile = '',
                ):
        #build command
        TCPPrepCommand = ['tcpprep']
        for Arg in Args:
            TCPPrepCommand.append(Arg)
        TCPPrepCommand.append('--cachefile=%s'%CacheFile)
        TCPPrepCommand.append('--pcap=%s'%PcapFile)

        myCommandString = ''
        for Command in TCPPrepCommand:
            myCommandString += Command + ' '
        print 'will now run command: %s' %myCommandString
        
        #run command
        try:
            self.TCPPrepCommand = Popen(TCPPrepCommand)
            sleep(1)
        except Exception as Error:
            raise Exception     
    
     
