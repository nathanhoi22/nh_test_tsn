import os
from subprocess import Popen, PIPE
from MyThread import *
from MyLogger import *
from SSHController import *
from time import sleep
#filesList
FilesListS = 'FilesList.txt'


class LiveTrafficServer():
    def __init__(self):
        self.cont =0
        self.sshcontrol = SSHController()
        self.TestFiles = []
        self.SetFilesListServer()
    def SetFilesListServer(self,
                           filesList = FilesListS):
        if os.path.exists(filesList) == False:
            Log('no files in list')
        else:
            for line in open(filesList, 'r').readlines():
                self.TestFiles.append(line.strip('\n').strip(' '))
            return self.TestFiles
    def start_connection(self, MACHINE = ''):
        self.sshcontrol.ConnectObjects[MACHINE] = self.sshcontrol.connect_to_machine(machine = MACHINE)
        assert self.sshcontrol.cont == 0, 'failed to connect to client machine'
    def Get_Server_User(self, MACHINE = ''):
        assert MACHINE != '', 'invalid machine entry'
        self.parse = MyXMLParser(MACHINE)
        server,user,password = self.parse.SSHClients[MACHINE]
        return user
    def Get_Server_Password(self, MACHINE = ''):
        assert MACHINE != '', 'invalid machine entry'
        self.parse = MyXMLParser(MACHINE)
        server,user,password = self.parse.SSHClients[MACHINE]
        return password
    def Get_Server_Addr(self, MACHINE = ''):
        assert MACHINE != '', 'invalid machine entry'
        self.parse = MyXMLParser(MACHINE)
        server,user,password = self.parse.SSHClients[MACHINE]
        return server
    def put_files_in_dir(self,
                         MACHINE = '',
                         LocalDir = '',
                         RemoteDir = '',
                         ):
        assert MACHINE != '', 'invalid machine entry'
        assert LocalDir != '', 'invalid machine entry'
        assert RemoteDir != '', 'invalid machine entry'
        #verify that RemoteDir is there
        self.sshcontrol.make_directory(machine = MACHINE,
                                       Dir = RemoteDir,
                                       )
        
        #check if files are already there
        run = 0
        for File in self.TestFiles:
            if os.path.exists('%s/%s'%(RemoteDir,File)):
                run += 0
            else:
                run += 1
                br
        if run > 0:
            self.sshcontrol.ConnectObjects[MACHINE].put_all(LocalDir,RemoteDir)
        else:
            Log('files exist, no need to replace dir')
    def start_python_server(self,
                            MACHINE = '',
                            RemoteDir = '',
                            port = None):
        assert MACHINE != '', 'invalid machine entry'
        assert RemoteDir != '', 'invalid machine entry'
        assert port != None, 'invalid machine entry'
        
        MyCommand = 'cd %s; python SimpleHTTPServerWithUploadWithPut.py %s' %(RemoteDir, str(port))
        
        #make it robot friendly
        remotepath = RemoteDir.encode()
        port = int(port)
        Log('planning to start python server at %s at port %d' %(remotepath, port))
        #is twistd server running
        if CommandValidator(self.sshcontrol.ConnectObjects[MACHINE], 'pgrep python', None) == 0:
            twistdIsRunning = 0
        else:
            twistdIsRunning = 1
            log('python server is running')
                        
        
        #kill server
        if twistdIsRunning == 1:
            log('killing python server')
            killtwisted = CommandValidator(self.sshcontrol.ConnectObjects[MACHINE],
                                           'kill `pgrep python`', None)
            assert killtwisted == 0, 'failed to kill python'
                    
                        
        #Thread this because it will run forever
        #start webserver
        Log ('starting Web Server')
        try:
            Log('Running python command: %s' %MyCommand)
            threadwebserver = MyThread( myfunc = self.sshcontrol.ConnectObjects[MACHINE].sendcommand,
                                        args = str(MyCommand),
                                        )  

            threadwebserver.run(timeout = 3) #give server 3 seconds to kick off
            failed = 0
        except NameError as error:
            failed = 0
            Log('dont fail on timeout error, this thread will hang if you dont exit') 
        except Exception as error:
            failed = 1
            Log('something else went wrong')
        assert failed == 0, 'failed to start webserver because %s' %error
    def stop_python_server(self,
                           MACHINE = '',
                           ):
        Log('killing python server')
        i,o,e = self.sshcontrol.ConnectObjects[MACHINE].sendcommand('kill `pgrep python`')
    def close_connection(self,
                         MACHINE = ''):
        self.sshcontrol.close_connection(machine = MACHINE)  

