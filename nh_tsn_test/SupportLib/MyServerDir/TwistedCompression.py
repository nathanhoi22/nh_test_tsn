'''
need newer twisted
    wget http://twistedmatrix.com/Releases/Twisted/12.1/Twisted-12.1.0.tar.bz2

Extract files and install

    tar jxvf Twisted-12.1.0.tar.bz2
    cd Twisted-12.1.0
    python setup.py install

    may need
    python-devel
    libevent-devel
    gcc
'''

#!/usr/bin/env python
#!/usr/local/lib/python2.7/site-packages/twisted

import sys, getopt

from twisted.web.server import Site, GzipEncoderFactory
from twisted.web.resource import Resource, EncodingResourceWrapper
from twisted.web import static
from twisted.internet import reactor
defaultpath = '.'
defaultport = 80

class WebServer(static.File):
    
    def getChild(self, path, request):
        child = static.File.getChild(self, path, request)            
        return EncodingResourceWrapper(child, [GzipEncoderFactory()])

def main(argv):
    cont = 1
    Method = ''
    try:
        opts, args = getopt.getopt(argv, 'f:p:h',['path=','port=', 'help='])
    except getopt.GetoptError:
        print 'failed to run error'
    path = defaultpath
    port = defaultport
    for opt, arg in opts:
        if opt == '-f':
            path = arg
        if opt == '-p':
            port = int(arg)
        if opt == '-h':
            cont = 0
        
    if cont == 1:
        print 'running twisted server on path %s on port %s' %(path, str(port))
        resource = WebServer(path)
        #wrapped = EncodingResourceWrapper(resource, [GzipEncoderFactory()])
        site = Site(resource)
        reactor.listenTCP(port, site)
        reactor.run()
    else:
        print 'syntax: python twistedCompresion.py -f <filepath> -p <port>'


    
if __name__=="__main__":
    main(sys.argv[1:])
###client needs to run
#curl --header 'accept-encoding: gzip' http://localhost/path 
