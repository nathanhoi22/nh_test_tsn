
###run this with root permissions

for f in `ls MALICIOUS_FILES`
do
	echo $f
	## do zip
	zip MALICIOUS_COMPRESSED/$f.zip MALICIOUS_FILES/$f
	## now do bzip
	cp MALICIOUS_FILES/$f MALICIOUS_COMPRESSED/$f	
	bzip2 MALICIOUS_COMPRESSED/$f
	## now do gzip
	cp MALICIOUS_FILES/$f MALICIOUS_COMPRESSED/$f	
	gzip MALICIOUS_COMPRESSED/$f
	## now do tar
	tar --create --file=MALICIOUS_COMPRESSED/$f.tar MALICIOUS_FILES/$f
	## now do 7z
	7z a MALICIOUS_COMPRESSED/$f.7z MALICIOUS_FILES/$f 
	## now do rar
	rar a MALICIOUS_COMPRESSED/$f.rar MALICIOUS_FILES/$f 
done

for f in `ls BENIGN_FILES`
do
	echo $f
	## do zip
	zip BENIGN_COMPRESSED/$f.zip BENIGN_FILES/$f
	## now do bzip
	cp BENIGN_FILES/$f BENIGN_COMPRESSED/$f	
	bzip2 BENIGN_COMPRESSED/$f
	## now do gzip
	cp BENIGN_FILES/$f BENIGN_COMPRESSED/$f	
	gzip BENIGN_COMPRESSED/$f
	tar --create --file=BENIGN_COMPRESSED/$f.tar BENIGN_FILES/$f
	## now do tar
	7z a BENIGN_COMPRESSED/$f.7z BENIGN_FILES/$f
	## now do rar
	rar a BENIGN_COMPRESSED/$f.rar BENIGN_FILES/$f
done
