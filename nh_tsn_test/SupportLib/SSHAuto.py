#!/usr/bin/env python

from paramiko import SFTPClient, SSHClient, AutoAddPolicy, Transport
import paramiko
from scp import SCPClient
from MyLogger import *
import os
from subprocess import Popen, PIPE


class MySSHClient():
    def __init__(self,server,port,user,password):
        self.ssh = SSHClient()
        self.ssh.load_system_host_keys()
        self.ssh.set_missing_host_key_policy(AutoAddPolicy())
        self.ssh.connect(server,port,user,password)
        self.scp = SCPClient(self.ssh.get_transport())
        self.sftp = SFTPClient.from_transport(self.ssh.get_transport())
    def getscp(self,
               LocalFile = None,
               RemoteFile = None):
        try:
            self.scp.get(RemoteFile, LocalFile)
        except Exception as error:
            Log('scp get error: %s' %error)
        
    def putscp(self,
               LocalFile = None,
               RemoteFile = None):
        try:
            self.scp.put(LocalFile, RemoteFile)
        except Exception as error:
            Log('scup put error: %s' %error)
            raise error
    def getsftp(self,
                LocalFile = None,
                RemoteFile = None):
        try:
            self.sftp.get(remotepath = RemoteFile, localpath=LocalFile)
        except Exception as error:
            Log('sftp get error: %s' %error)
            raise error
    def putsftp(self,
                LocalFile = None,
                RemoteFile = None):
        try:
            self.sftp.put(remotepath = RemoteFile, localpath=LocalFile)
        except Exception as error:
            Log('sftp put error: %s' %error)
            raise error
    def readfile(self,
                 RemoteFile= None,
                 ):
        if RemoteFile:
            f = self.sftp.open(RemoteFile, 'r')
            readlines = f.readlines()
            f.close
            return readlines
        else:
            return []
    def put_all(self,
                localpath, #must exist
                remotepath, #must exist end in /
                ):
        #remove all files from dir
        Log('first remove all files in remote path')
        self.sendcommand('cd %s; rm -rf *' %remotepath)
        
        #create dir list
        Log('now create dir list', Indent = 2)
        DirList = []
        for root,dirs,files in os.walk(localpath):
            #lets remove first folder from root
            for Dir in dirs:
                DirList.append(Dir)
                Log(Dir, Indent = 4)
        
        
        #create files List
        Log('now create Files list', Indent = 2)
        FileList = []
        for root,dirs,files in os.walk(localpath):
            #lets remove first folder from root
            for File in files:
                myfilestring = '%s/%s' %(root,File)
                    #now strip the './' from begining of string
                myfilestring = myfilestring[len(localpath)+1:]
                FileList.append(myfilestring)
                Log(myfilestring, Indent = 4)

        
        #now create Dirs
        Log('create all dirs needed', Indent = 4)
        for Dir in DirList:
            Log('creating dir %s/%s' %(remotepath,Dir), Indent = 6)
            self.sftp.mkdir('%s/%s' %(remotepath,Dir))
            
        #now copy file
        Log('copy all files needed', Indent = 4)
        for File in FileList:
            log ('copying %s/%s to %s/%s' %(localpath,File,remotepath,File), Indent = 6)
            self.putsftp('%s/%s' %(localpath,File),'%s/%s' %(remotepath,File)) 
           

    def sendcommand(self, command):
        Log('executing %s' %command)
        ssh_stdin, ssh_stdout,ssh_stderr = self.ssh.exec_command(command)
        out = []
        err = []
        for line in ssh_stderr.readlines():
            try:
                line = line.encode()
                err.append(line) 
                Log('Error line: %s' %line)
            except UnicodeEncodeError:
                pass ## ignore unicode errors
            
        for line in ssh_stdout.readlines():
            try:
                line = line.encode()
                Log('Output line: %s' %(line))
                out.append(line)
            except UnicodeEncodeError:
                pass ## ignore unicode errors
        return ssh_stdin,out,err
    
    def close(self):
        self.sftp.close()
        self.ssh.close()

def MyTest(server,
           port,
           user,
           password,
           transferfileDir,
           transferfile,
           transferfilerename,
           remotedir):
    '''
    test will
    put file via scp and sftp(two different tests)
    get file as different name
    compare two files verifing they are the same
    '''
    nRet = 0

    #set file variables
    if nRet == 0:
        lf = transferfileDir + '/' + transferfile
        lf2 = transferfileDir + '/' + transferfilerename
        rf = remotedir + '/' +transferfile

    #check if file exists
    nRet = 0
    if nRet == 0:
        if os.path.exists(lf) == False:
            nRet == 1
            Log('cant transfer %s because it does not exist' %lf)

    #connect      
    if nRet ==0:
        MYSSHCL = MySSHClient(server, port, user, password)
        

    #put file scp
    if nRet == 0:
        try:
            MYSSHCL.putscp(LocalFile = lf, RemoteFile = rf)
        except Exception as error:
            nRet = 1
            Log(error)

    #get file scp
    if nRet == 0:
        try:
            MYSSHCL.getscp(LocalFile = lf2, RemoteFile = rf)
        except Exception as error:
            nRet = 1
            Log(error)


    nRet = Validate2files(lf, lf2)

    if nRet == 0:
        Log('success. scp put and get returned same file')
    else:
        Log('Failure. unsuccessful scp transfer.  see logs return value = %d' %nRet)

    #remove renamed file
    if nRet == 0:
        ReMoveFileCommand = ['rm', '-rf', 'lf2']
        try:
            Remove = Popen(ReMoveFileCommand, stdout=PIPE)
        except Exception as error:
            Log(error)
            nRet = 1
        for line in Remove.stdout.readlines():
            Log(line)
        
        
            
    #put file ftp
    if nRet == 0:
        try:
            MYSSHCL.putsftp(LocalFile = lf, RemoteFile = rf)
        except Exception as error:
            nRet = 1
            Log(error)

    #get file ftp
    if nRet == 0:
        try:
            MYSSHCL.getsftp(LocalFile = lf2, RemoteFile = rf)
        except Exception as error:
            nRet = 1
            Log(error)   

    nRet = Validate2files(lf, lf2)

    if nRet == 0:
        Log('success. sftp put and get returned same file')
    else:
        Log('Failure. unsuccessful sftp transfer.  see logs return value = %d' %nRet)

    #remove renamed file
    if nRet == 0:
        ReMoveFileCommand = ['rm', '-rf', 'lf2']
        try:
            Remove = Popen(ReMoveFileCommand, stdout=PIPE)
        except Exception as error:
            Log(error)
            nRet = 1
        for line in Remove.stdout.readlines():
            Log(line)
'''
ex:     
MyTest('65.202.129.220',
       22,
       'testing',
       'PA$$word11',
       '/home/testing/Myvms',
       'Testing.txt',
       'testingrename.txt',
       '/home/testing')
'''       
    
