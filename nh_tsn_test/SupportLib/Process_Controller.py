## this is for creating commands to controll processes
#!/usr/bin/env python

from subprocess import Popen, PIPE
from SSHController import *

class ProcessController():
    def __init__(self):
        self.cont = 0
        self.RunningProcs = {} #return list of processed, pids, and child pids
        self.sshcontrol = SSHController()

    def start_connection(self, machine):
        self.sshcontrol.ConnectObjects[self.machine] = self.sshcontrol.connect_to_machine(machine = MACHINE)
        assert self.sshcontrol.cont == 0, 'failed to connect to client machine'
    
    @classmethod
    def GetProcs(self,
                 machine = 'local'):
        
        #first use ps -ef to get all running processes
        if machine == 'local':  #command to run if local machine
            Procs = Popen('ps -ef', executable = '/bin/bash', stdout=PIPE, shell = True).stdout.readlines()
        else:  #command to run on remote machine
            exists = 0
            for key in self.sshcontrol.ConnectObjects:
                if machine == key:
                    exists = 1
                    Input,Output, Error = self.sshcontrol.ConnectObjects[machine.encode()].sendcommand('ps -ef')
                    Procs = Output
                    break
            if exists == 0:
                print 'machine not connected, make sure you run the connection first'
           
        #####

        self.RunningProcs = {}
        #now parse the output and return process name, parent pid and child pid
        for proc in Procs:
            #remove extra spaces
            proc = proc.split(' ')
            nproc = []
            for split in proc:
                if split != '':
                    nproc.append(split)
            proc = nproc
            if len(proc) < 8:
                print 'something went wrong'
            else:
                Pid = proc[1]
                Ppid = proc[2]
                #command build from last after 7
                #Names = []
                for i in range(7,len(proc)):
                    self.RunningProcs[proc[i].strip('\n')] = [Pid, Ppid]
                    #Names.append(proc[i].strip('\n'))
            
        return self.RunningProcs
            
    def killProcess(self, pid):
        #first use ps -ef to get all running processes
        if machine == 'local':  #command to run if local machine
            kill = Popen('kill %d' %pid, executable = '/bin/bash', stdout=PIPE, shell = True).stdout.readlines()
        else:  #command to run on remote machine
            exists = 0
            for key in self.sshcontrol.ConnectObjects:
                if machine == key:
                    exists = 1
                    Input,Output, Error = self.sshcontrol.ConnectObjects[machine.encode()].sendcommand('kill %d' %pid)
                    kill = Output
                    break
            if exists == 0:
                print 'machine not connected, make sure you run the connection first'
           
        #####


            
