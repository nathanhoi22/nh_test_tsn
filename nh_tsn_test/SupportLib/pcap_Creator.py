'''
###SETUP FOR SERVER####
1. setup twisted server
#install
yum -y install twisted python
#start
twist web --path <MyServerDir> --port 80

2. stop iptables
service iptables stop

3. install/configure vsftpd server
#install
yum -y install vsftpd
#configure /etc/vsftpd/vsftpd.conf
#verify
local_enable=YES
write_enable=YES
#add
local_root= <MyServerDir>
#Remove root in /etc/vsftpd/ftpusers and /etc/vsftpd/user_list to allow root logon
#set ftp full directory access
setsebool -P allow_ftpd_full_access 1
#start service
sevice vsftpd start




###Client (place to run test)
#need python, and python-pip, curl, tcpdump and python modules paramiko and scp
yum -y install python curl tcpdump
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum install python-pip
pip install paramiko
pip install scp

###Need scripts from SupportLib dir
'''


'''For 2.0 support
# To clone the nghttp2 Github repo
yum install git

# Build essentials
yum install gcc
yum install make
yum install automake
yum install libtool

# Required to build the library
yum install pkgconfig
yum install zlib-devel

Once done clone the repo:

git clone https://github.com/tatsuhiro-t/nghttp2.git
cd nghttp2

Build the library as explained here:

autoreconf -i
automake
autoconf
# Note: I assume you want to deploy it under /usr/local
# Feel free to adapt to your needs!
./configure --prefix=/usr/local
make

Then deploy it:

make install

If everything is OK you then need to build libcurl 7.33.0 by taking care to enable nghttp2 with ./configure --with-nghttp2=/usr/local [...].

Extras

If you want to build the application programs in addition (nghttp, ...) you would have to install additional packages before building nghttp2:

yum install openssl-devel
yum install libevent-devel
yum install libxml2-devel
yum install jansson-devel

'''
#filesList
FilesListC = 'FilesList.txt'

import os
from subprocess import Popen, PIPE
import threading
from MyLogger import *
from SSHController import *
from time import sleep

xmlserverentry = 'server'
MyPCAPDir = 'PCAPDIR'
MyFTPUser = 'root'
MyFTPPass = 'password'
MyServer = '192.168.1.4'
MyServerDir = '/root/files/Files'
MyLocalFilesDir = 'LOCALDIR'

Methods = ['HTTP_GET',
           'HTTP_POST',
           'HTTP_PUT', 
           'FTP_GET',
           'FTP_PUT', #run last
           'FTP_APPEND', #run last
           ]
HTTPVERSIONS = ['1.0',
                '1.1',
                #'2.0', 
                ]
COMPRESSIONS = [None,
                #'gzip',
                ]

                

class  Pcap_Creator():
    def __init__(self):
        self.cont = 0
        self.tcpdump = None
        self.Setup_init()
        self.TestFiles = []
        self.SetFilesListClient()
        

    def SetFilesListClient(self,
                           filesList = FilesListC):
        if os.path.exists(filesList) == False:
            Log('no files in list')
        else:
            for line in open(filesList, 'r').readlines():
                self.TestFiles.append(line.strip('\n').strip(' '))
            return self.TestFiles

    def Setup_init(self):
        if os.path.exists(MyPCAPDir) == False:
            try:
                Log ('making dir %s' %MyPCAPDir)
                MyCommand = ['mkdir']
                MyCommand.append(MyPCAPDir)
                s = Popen(MyCommand, stdout=PIPE)
            except Exception as error:
                Log('error : %s' %error)
                for Line in s.stdout.readlines():
                    Log(Line)
            
        if os.path.exists(MyLocalFilesDir) == False:
            try:
                Log ('making dir %s' %MyLocalFilesDir)
                
                MyCommand = ['mkdir']
                MyCommand.append(MyLocalFilesDir)
                s = Popen(MyCommand, stdout=PIPE)
            except Exception as error:
                Log('error : %s' %error)
                for Line in s.stdout.readlines():
                    Log(Line)
                    
        
    def runTcpDump(self,
                   fileName,  
                   Method,
                   eth = 'eth0',
                   HttpVer = '1.1',
                   Encoding = None,
                   ):
        
        #build command
        MyCommand = ['tcpdump'] #start command
        MyCommand.append('-i %s' %eth) #interface
        if 'http' in Method.lower():
            pcapname = '%s/%s_vers%s_%s_enc%s.pcap' %(MyPCAPDir, os.path.splitext(fileName)[0],HttpVer,Method, str(Encoding))
        else:
            pcapname = '%s/%s_%s_enc%s.pcap' %(MyPCAPDir, os.path.splitext(fileName)[0],Method,str(Encoding))
            
        MyCommand.append('-w %s' %pcapname)
        CommandString = ''
        for Command in MyCommand:
            CommandString += Command + ' '
        Log('Running %s' %CommandString)
        
        #thread Command
        self.tcpdump = Popen(CommandString, executable='/bin/bash', shell = True)
        #tcpdumpthread = threading.Thread(target =Popen, args = (MyCommand, shell=True,))
        #tcpdumpthread.start()
        return self.tcpdump
    
    

    def killTcpDump(self):
        #if self.tcpdump:
        #    self.tcpdump.kill()
        
        try:
            MyCommand = ['kill']
            MyCommand.append('`pgrep tcpdump`')
            CommandString = ''
            for Command in MyCommand:
                CommandString += Command + ' '
            Log('Running %s' %CommandString)
            kill = Popen(CommandString, executable='/bin/bash', shell = True)
            #Popen(MyCommand)
            nRet =0
        except Exception as error:
            if 'usage: ' in error:
                #already killed
                pass
            else:
                Log('tcpdump was not running. something went wrong : %s' %error)
                nRet = 1
                
        return nRet
        
    def runCurl(self,
                fileName,
                Method,
                HttpVer = '1.1', #1.0, 2
                Encoding = None,
                waittime = 4, #sometimes if tcpdump is killed too quickly it will have packets filterd
                ):
        
        sleep(waittime)
        MyCommand = ['curl'] #start command
        PROTO = Method.split('_')[0].lower()
        Method = Method.split('_')[1]
        
            
        MyCommand.append('--request %s' %Method)
        if PROTO == 'http':
            if HttpVer == '1.0':  #SUPPORT FOR 2.0 LATER
                MyCommand.append('-0')
            elif HttpVer == '2.0':
                MyCommand.append('--http2')
            port = 80
            if Method.lower() == 'put' or Method.lower() == 'post':
                MyCommand.append('-i')
                MyCommand.append('--form "file=@%s/%s;filename=put_%s" --form file=upload' %(MyLocalFilesDir, os.path.basename(fileName), os.path.basename(fileName)))
                MyCommand.append('%s://%s:%s'%(PROTO,MyServer,port))
            else:
                if Encoding != None:
                    MyCommand.append("--header 'accept-encoding:%s'" %Encoding)
                    MyCommand.append('%s://%s:%s/%s'%(PROTO,MyServer,port,fileName))
                    MyCommand.append(' > %s.%s' %(os.path.basename(fileName),Encoding))
                else:
                    MyCommand.append('%s://%s:%s/%s'%(PROTO,MyServer,port,fileName))
                    MyCommand.append(' > %s' %os.path.basename(fileName))
                
                                 
                                
        elif PROTO == 'ftp':
            port = 21
            MyCommand.append('--user %s:%s' %(MyFTPUser,MyFTPPass))
            if Method.lower() == 'put' or Method.lower() == 'append':
                if Method.lower() == 'append':
                    MyCommand.append('--append')
                if Encoding == None:
                    MyCommand.append('-T %s/%s' %(MyLocalFilesDir, os.path.basename(fileName)))
                else:
                    MyCommand.append('-T %s/%s.%s' %(MyLocalFilesDir, os.path.basename(fileName), Encoding))
                MyCommand.append('%s://%s/put_%s'%(PROTO,MyServer,os.path.basename(fileName)))
            else:
                MyCommand.append('%s://%s/%s'%(PROTO,MyServer,fileName))
                if Encoding == None:
                    MyCommand.append(' > %s' %os.path.basename(fileName)) #save output to filename
                else:
                     MyCommand.append(' > %s.%s' %(os.path.basename(fileName),Encoding)) #save output to filename
        else:
            raise ValueError
            Log( 'PROTO should be ftp or http but is %s' %PROTO )
        CommandString = ''
        for Command in MyCommand:
            CommandString += Command + ' '
        Log('running %s' %CommandString)
        try:
            Log('running %s' %CommandString)
            s = Popen(CommandString, executable = '/bin/bash', stdout=PIPE, shell = True)
        except Exception as error:
            Log('error : %s' %error)
        for Line in s.stdout.readlines():
                    Log(Line)
        sleep(waittime)
   
    def cleanup(self):
        try:
            Log('running rm -rf %s' %MyLocalFilesDir)
            s = Popen('rm -rf %s' %MyLocalFilesDir, executable = '/bin/bash', stdout=PIPE, shell = True)
                
        except Exception as error:
            Log('error : %s' %error)
        for Line in s.stdout.readlines():
                    Log(Line)
                    
def RunTest():
    self = Pcap_Creator()
    nRet = 0
    for Method in Methods:
        for fileName in self.TestFiles:
            for HTTPVERSION in HTTPVERSIONS:
                for COMPRESSION in COMPRESSIONS:
                    skiprun = 0
                    putRun = 0
                    if (HTTPVERSION != '1.1') and ('ftp' in Method.lower()):
                        skiprun = 1
                    if (COMPRESSION != None) and ('ftp' in Method.lower()):
                        skiprun = 1
                    if Method == 'FTP_PUT' or Method == 'FTP_APPEND':
                        putRun = 1
                    if skiprun == 0:
                        Log("RUNNIG METHOD=%s HTTP%s COMPRESSION%s"  %(Method, HTTPVERSION, COMPRESSION))
                        #removing file if first time launch and file exists
                        if os.path.exists(fileName):
                            try:
                                Log('now removing file')
                                os.remove(os.path.basename(fileName))
                            except:
                                Log ('failed to remove file')
                                nRet =-1
                                return nRet
                        
                        ##start tcpdump
                        Log ( 'starting tcp dump' )
                        self.runTcpDump(os.path.basename(fileName),
                                        Method,
                                        eth = 'eth0',
                                        HttpVer = HTTPVERSION,
                                        Encoding = COMPRESSION,
                                        )
                                          
                        #Get the file via Method
                        Log( 'runing curl' )
                        self.runCurl(fileName,
                                     Method,
                                     HttpVer = HTTPVERSION,
                                     Encoding = COMPRESSION,
                                     )
                                          
                                     
                        ##Now kill the TCPDump
                        Log(' killing tcpdump ')
                        self.killTcpDump()
                                          


                        #ONLY VERIFY MD5 if compression not used
                        if COMPRESSION == None:
                            ##now verify if file on server machine matches file on local
                            self.sshcontrol = SSHController()
                            self.sshcontrol.connect_to_machine(xmlserverentry)
                            Log ('running file validation')
                            if 'get' in Method.lower():
                                if self.sshcontrol.RunFileValidation(File1 = [os.path.basename(fileName), 'local'],
                                                                     File2 = ['%s/%s' %(MyServerDir,fileName), xmlserverentry],
                                                                     ) != 0:
                                    Log('file not match %s:%s -- $s%s' %('local', fileName, MyServerDir,fileName))
                                    nRet = 1
                                    return nRet
                                else:
                                    Log('file successfully copied')
                            else:
                                if self.sshcontrol.RunFileValidation(File1 = ['%s/%s' %(MyLocalFilesDir, os.path.basename(fileName)), 'local'],
                                                                     File2 = ['%s/put_%s' %(MyServerDir,os.path.basename(fileName)), xmlserverentry],
                                                                     ) != 0:
                                    Log('file not match %s:%s -- $s%s' %('local', fileName, MyServerDir,fileName))
                                          
                                    nRet = 1
                                    return nRet
                                #Now remove the put files
                                self.sshcontrol.ConnectObjects[xmlserverentry].sendcommand('rm -rf %s/put_%s' %(MyServerDir,os.path.basename(fileName)))

                                    
                        #no files if put run
                        ##copy files to localdir
                        if putRun == 0:
                            try:
                                if COMPRESSION == None:
                                    Log('compression is none')
                                    Log('running cp %s %s/%s' %(os.path.basename(fileName), MyLocalFilesDir, os.path.basename(fileName) ))
                                    s = Popen('cp %s %s/%s'%(os.path.basename(fileName), MyLocalFilesDir, os.path.basename(fileName) ), executable = '/bin/bash', stdout=PIPE, shell = True)
                                else:
                                    Log('compression is %s' %COMPRESSION)
                                    Log('running cp %s.%s %s/%s.%s' %(os.path.basename(fileName), COMPRESSION, MyLocalFilesDir, os.path.basename(fileName), COMPRESSION))
                                    s = Popen('cp %s.%s %s/%s.%s'%(os.path.basename(fileName), COMPRESSION, MyLocalFilesDir, os.path.basename(fileName), COMPRESSION), executable = '/bin/bash', stdout=PIPE, shell = True)
                                
                                    
                            except Exception as error:
                                Log('error : %s' %error)
                            for Line in s.stdout.readlines():
                                        Log(Line)
                                    
                            #because you are capturing pcap, you dont need the file anymore
                            #removing file
                            try:
                                if COMPRESSION == None:
                                    Log('now removing file locally')
                                    os.remove(os.path.basename(fileName))
                                else:
                                    Log('now removing file locally')
                                    os.remove('%s.%s' %(os.path.basename(fileName),COMPRESSION))
                            except:
                                Log ('failed to remove file')
                                nRet =2
                                #return nRet
    print nRet
    return nRet
            
RunTest()
'''
#to test tcpdump start/stop functionality
self = Pcap_Creator()
self.runTcpDump('testing.txt', 'HTTP_GET')
self.runCurl('testing.txt', 'HTTP_GET')
self.killTcpDump()
'''
