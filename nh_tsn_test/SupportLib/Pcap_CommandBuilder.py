import os


class Pcap_Command_Builder():
    def __init__(self, pcapfile,):
        self.cont = 0
        self.PcapFile = pcapfile

    def mod_dump_command(self,
                         ports = [], #array
                         eth = 'eth0',
                         saveas = None):
        if saveas == None:
            saveas = self.PcapFile
        MyCommand = 'tcpdump -s 0 '
        if len(ports) != 0:
            addportsCommand = 'port '
            for i in range(len(ports)):
                if i == len(ports) -1:
                    addportsCommand += '%s ' %str(ports[i]) + ' '
                else:
                    addportsCommand += '%s or' %str(ports[i]) + ' '
            MyCommand += addportsCommand
        MyCommand += '-i %s ' %eth
        MyCommand += '-w %s' %saveas
        return MyCommand

        
    def mod_pcap_command (self,
                          SourceMAC = None,
                          DestMac = None,
                          SourceIP = None,
                          DestIP = None,
                          VlanTagID = None,
                          RemapUsed = False, #80
                          RemapNew = 8888,
                          error = None,
                          FilterPacketNumbers = ['exclude',
                                                 None, #this is an array
                                                 ],
                          AdvanceTimeStamp = None,
                          mtu = 1500,
                          ):
        skipBroad = 0
        InFile = self.PcapFile
        CFile = os.path.splitext(self.PcapFile)[0] + '.cache'
        OutFile1 = os.path.splitext(self.PcapFile)[0] + '_1.pcap'
        OutFile2 = os.path.splitext(self.PcapFile)[0] + '_2.pcap'
        OutFile3 = os.path.splitext(self.PcapFile)[0] + '_3.pcap'
        OutFile4 = os.path.splitext(self.PcapFile)[0] + '_4.pcap'
        OutFile5 = os.path.splitext(self.PcapFile)[0] + 'final.pcap'
        CommandArgs1 = ['tcpprep --port',
                        '--cachefile=%s' %CFile,
                        '--pcap=%s' %InFile,
                        ]
        CommandArgs2 = ['tcprewrite']
        if SourceMAC != None and DestMac != None:
            addArgs = ['--enet-dmac=%s' %DestMac,
                       '--enet-smac=%s' %SourceMAC,
                       ]
            CommandArgs2 += addArgs
            skipBroad = 1
        
        if DestIP != None and SourceIP != None:
            CommandArgs2.append('--endpoints=%s:%s'%(SourceIP,DestIP))
            skipBroad = 1
        if skipBroad == 1:
            CommandArgs2.append('--skipbroadcast')
        if VlanTagID == None:
            CommandArgs2.append('--enet-vlan=del')
        else:
            addArgs = ['--enet-vlan=add',
                       '--enet-vlan-tag=%d' %VlanTagID,
                       '--enet-vlan-cfi=1',
                       '--enet-vlan-pri=4',
                       ]
            CommandArgs2 += addArgs
        if RemapUsed != False:
            CommandArgs2.append(' --portmap=%s:%s' %(RemapUsed,RemapNew))
        
        addArgs = ['--cachefile=%s'%CFile,
                   '--infile=%s'%InFile,
                   '--outfile=%s'%OutFile1,
                   ]
        CommandArgs2 += addArgs

        
        CommandArgs3 = []
        if FilterPacketNumbers[1] != None and AdvanceTimeStamp != None:
            CommandArgs3 = ['editcap']
            if AdvanceTimeStamp != None:
                CommandArgs3.append('-e %s'%AdvanceTimeStamp)
            if FilterPacketNumbers[1] != None:
                if FilterPacketNumbers[0].lower() == 'include':
                    CommandArgs3.append('-r')
                addArgs= [OutFile1,
                          OutFile2]
                CommandArgs3 += addArgs
            for packet in FilterPacketNumbers[1]:
                CommandArgs3.append(packet)
                    
        if len(CommandArgs3) == 0:
            OutFile2 = OutFile1
            
            
        CommandArgs4 = ['tcpewrite',
                        '--fixlen=trunc',
                        '--infile=%s'%OutFile2,
                        '--outfile=%s'%OutFile3,
                        ]
        CommandArgs5 = ['tcpewrite',
                        '--mtu=%d'%mtu, #set mtc
                        '--mtu-trunc',
                        '--infile=%s'%OutFile3,
                        '--outfile=%s'%OutFile4,
                        ]
        CommandArgs6 = ['tcpewrite',
                        '--fixcsum',
                        '--infile=%s'%OutFile4,
                        '--outfile=%s'%OutFile5,
                        ]

        #now make command
        MyRunCommand = ''
        for Command in CommandArgs1:
            MyRunCommand += Command + ' '
        MyRunCommand += '; '
        
        for Command in CommandArgs2:
            MyRunCommand  += Command + ' '               
        MyRunCommand += '; '

        if len(CommandArgs3) != 0:
            Command3 = ''
            for Command in CommandArgs3:
                MyRunCommand += Command + ' ' 
            MyRunCommand += '; '

        for Command in CommandArgs4:
            MyRunCommand  += Command + ' ' 
        MyRunCommand += '; '
        for Command in CommandArgs5:
            MyRunCommand  += Command + ' ' 
        MyRunCommand += '; '
        for Command in CommandArgs6:
            MyRunCommand  += Command + ' '
            
        return MyRunCommand
    def mod_replay_command(self,
                           pcap = None,
                           TopSpeed = False,
                           Speed = None, #100.0
                           Loop = False,
                           IntF = ['eth0'],
                           ):
        assert pcap != None, 'need pcap file'
        MyRunCommand = 'tcpreplay '
        for i in range(len(IntF)):
            MyRunCommand += '--intf%d=%s ' %(i+1,IntF[i]) 
        if TopSpeed == True:
            MyRunComand += '-t '
        if Speed !=None:
            MyRunCommand += '--mbps=%s ' %str(Speed)
        MyRunCommand += pcap

        return MyRunCommand
            
        

def test():
    p = Pcap_Command_Builder('PacketPlay/mypcap.pcap')
    mywriteCommand = p.mod_pcap_command(SourceMAC = '00:99:88:77:66:55',
                                   DestMac = '11:22:33:44:55:66',
                                   SourceIP = '1.1.1.1',
                                   DestIP = '1.1.1.2',
                                   VlanTagID = 3,
                                   RemapUsed = 80, #80
                                   RemapNew = 8888,
                                   error = '0.05',
                                   FilterPacketNumbers = ['exclude',['1',
                                                                     '3',
                                                                     '100-200',
                                                                     ],
                                                          ],
                                   AdvanceTimeStamp = 1,
                                   mtu = 1000,
                                   )
    print mywriteCommand
    myreplayCommand = p.mod_replay_command(pcap = 'mypcapfinal.pcap',
                                           Speed = '100.0',
                                           Loop = 10,
                                           IntF = ['eth0'])
    print '...'
    print myreplayCommand
    print '...'
    mydumpCommand = p.mod_dump_command(ports = [21,80], #array
                                       eth = 'eth0',
                                       saveas = None)
    print mydumpCommand
test()
