#!usr/bin/env python

from SSHController import *
import os


class TwistedTest():
	def __init__(self):
                self.cont = 0
                self.LocalTarFile = ''
                self.MyFilesList = []
                self.machine = '' #this is for defining your current working machine
                #start up controller
                self.sshcontrol = SSHController()
                assert self.sshcontrol.cont == 0, 'could not start controller'
        def set_tar_file(self,
                         LocalTarFile):
                #make it robot friendly
                LocalTarFile = LocalTarFile.encode()
                
                assert os.path.exists(LocalTarFile) == True, '%s does not exist' %LocalTarFile
                self.LocalTarFile = LocalTarFile
                
        def get_files_list_from_tar_file(self,
                                         message = ''):
                tar = tarfile.open(self.LocalTarFile)
                for f in tar.getmembers():
                        ('log appending %s to files fileslist' %f)
                        self.MyFilesList.append(f.name)
                assert len(self.MyFilesList) != 0, 'no files in tar file'
                Log('successfully listed files from Tarfile to MyFilesList %s' %message)
                return self.MyFilesList
	def start_connection(self, MACHINE = ''):
                self.sshcontrol.ConnectObjects[MACHINE] = self.sshcontrol.connect_to_machine(machine = MACHINE)
                assert self.sshcontrol.cont == 0, 'failed to connect to client machine'
	
	def unzip_tar_file(self,
                         machine = '',
                         RemoteTarFilepath = '',
                         ExpectedFiles = [], #array of files to expect to be unzipped
                         EXPECT = '',
                         ):
                #make it robot friendly:
                machine = machine.encode()
                RemoteTarFilepath = RemoteTarFilepath.encode()
                EXPECT = EXPECT.encode()
                ####################
                
                nRet, MACHINE = self.sshcontrol.VerifyMyMachine(machine)
                assert nRet == 0, "machine %s is not started as object" %MACHINE
                
                #go to dir and unzip files
                TarPath, TarFile= os.path.split(RemoteTarFilepath)
                if CommandValidator(self.sshcontrol.ConnectObjects[MACHINE],
                                    'cd %s; tar -xvzf %s' %(TarPath,TarFile),
                                    Expect = EXPECT) != 0:
                        Log('failed to extract tar files')
                        nRet = 1
                assert nRet == 0, 'failed to extract tar files'
                #verify files exist
                Input, Output, Error = self.sshcontrol.ConnectObjects[MACHINE].sendcommand('cd %s/%s ; ls' %(TarPath,ExpectedFiles[0])) #ExpectedFiles[0] is dir that files are installed into
                for File in ExpectedFiles:
                        Log('ckecking if %s in list' %os.path.basename(File))
                        if os.path.splitext(os.path.basename(File))[1] == '': #dont test direcotry
                                pass
                        else:
                                if os.path.basename(File) in str(Output):
                                        pass
                                else:
                                        Log('tar files are not properly extracted')
                                        nRet = 2

                
                assert nRet == 0, 'tar files are not properly extracted'
                Log('successfully extraced all files')
                                                        
        def run_client_wget_test(self,
                                 MACHINE = 'client',
                                 SERVER = 'server',
                                 machineDir = '.',
                                 serverDir = '',
                                 port = 8080,
                                 ValidationFilesList = 'Default'):
                #for Robot Friendliness
                MACHINE = MACHINE.encode()
                SERVER = SERVER.encode()
                machineDir = machineDir.encode()
                serverDir = serverDir.encode()
                port = int(port)

                
                print 'serverDir part1', serverDir
                print 'serverDir',  serverDir
                if ValidationFilesList == 'Default':
                        ValidationFilesList = self.MyFilesList
                assert len(ValidationFilesList) != 0, 'no files in the files list'
                
                #remove client files
                for f in ValidationFilesList:
                        remove = CommandValidator(self.sshcontrol.ConnectObjects[MACHINE],
                                                  'rm -rf %s' %os.path.basename(f),
                                                  None,
                                                  )
                        assert remove == 0, 'failed to remove file %s' %f
                
                #run wget on client machine
                for f in self.MyFilesList:
                        if os.path.splitext(f)[1] == '': #this is not a file
                                pass
                        else:
                                server,user,password = self.sshcontrol.parse.SSHClients[SERVER]
                                print server, user, password
                                wget =  CommandValidator(self.sshcontrol.ConnectObjects[MACHINE],
                                                    'wget http://%s:%d/%s' %(server,port, f),
                                                    'OK',
                                                    )
                                assert wget ==0, 'Failed to run wget on client machine'
                                       
                #Verify files on client machine
                I, Output, Error = self.sshcontrol.ConnectObjects[MACHINE].sendcommand('ls')
                for f in self.MyFilesList:
                        if os.path.splitext(f)[1] == '': #this is not a file
                                pass
                        else:
                                if os.path.basename(f) in str(Output):
                                        failed = 0
                                else:
                                        failed = 1
                                assert failed == 0, 'failed to find file on client machine after wget'

                #Verify mdfive of files
                for f in ValidationFilesList:
                        print 'validate machine dir: ', machineDir
                        print 'validate server dir: ', serverDir
                        validate = self.sshcontrol.RunFileValidation(File1 = ['%s/%s' %(machineDir,os.path.basename(f)), MACHINE],
                                                                     File2 = ['%s/%s' %(serverDir,os.path.basename(f)), SERVER],
                                                                     )
                        assert validate == 0, 'failed to validate files'
                        

                                
                #remove client files again
                for f in ValidationFilesList:
                        remove = CommandValidator(self.sshcontrol.ConnectObjects[MACHINE],
                                            'rm -rf %s' %os.path.basename(f),
                                            None,
                                            )
                        assert remove == 0, 'failed to remove file %s second time' %s
                                
                                                        
        def start_twistd_server(self,
                                MACHINE = 'server',
                                remotepath = '',
                                port = 8080):
                #make it robot friendly
                remotepath = remotepath.encode()
                port = int(port)
                Log('starting twistd server at %s at port %d' %(remotepath, port))
                #is twistd server running
                if CommandValidator(self.sshcontrol.ConnectObjects[MACHINE], 'pgrep twistd', None) == 0:
                        twistdIsRunning = 0
                else:
                        twistdIsRunning = 1
                                
                
                #kill twistd server
                if twistdIsRunning == 1:
                        killtwisted = CommandValidator(self.sshcontrol.ConnectObjects[MACHINE],
                                                       'kill `pgrep twistd`', None)
                        assert killtwisted == 0, 'failed to kill twistd'
                            
                                
                #Thread this because it will run forever
                #start webserver
                Log ('starting Web Server')
                try:
                        threadwebserver = MyThread( myfunc = self.sshcontrol.ConnectObjects[MACHINE].sendcommand,
                                                    args = 'twistd -n web --path %s --port %d' %(remotepath,port),
                                                    )  

                        threadwebserver.run(timeout = 3) #give server 3 seconds to kick off
                        failed = 0
                except NameError as error:
                        failed = 0
                        Log('dont fail on timeout error, this thread will hang if you dont exit') 
                except Exception as error:
                        failed = 1
                        Log('something else went wrong')
                assert failed == 0, 'failed to start webserver because %s' %error
        def close_connection(self,
                             MACHINE = ''):
                self.sshcontrol.close_connection(machine = MACHINE)



def Test():
        self = TwistedTest()
        Log('setting tar file')
        self.set_tar_file('./Files.tgz')
        Log('getting file list from tar file')
        ef = self.get_files_list_from_tar_file()
        for f in ef:
                Log ('found file %s' %f)
        Log('starting connection to server')
        self.start_connection(MACHINE = 'server')
        Log('uzipping tar file')
        self.unzip_tar_file(machine = 'server',
                            RemoteTarFilepath = '~/files/Files.tgz',
                            ExpectedFiles = ef,
                            EXPECT = ef[0],
                            )
#Test()
        
        
