*** Settings ***    Value
Library           SupportLib.robotlib
Library           SupportLib.LiveTrafficClient
Library           SupportLib.LiveTrafficServer

*** Test Case ***
Prepare the server machine
    ###For this test to work you need the server already set up with python
    #**************************************************************************************************************************************************************************
    Test Name    ${TEST NAME}
    #**************************************************************************************************************************************************************************
    # start up server Machine
    ${MACHINE}    Set Variable     server
    start connection    ${MACHINE}
    #Define MyServerDir
    #MyServerDir should be the host of all files on server
    #MyServerDir should be the local_root of vsftpd on server
    ${MyServerDir}   Set Variable    /root/files/Files
    
    #set port
    ${port}    Set Variable    80
    #Set MyServerDir files locally
    #this is needed to set up MyserverDir remotely
    ${MyServerDirLocal}  Set Variable   SupportLib/MyServerDir

    #set MyFiles List locally
    #this is needed for a verification check if server recieved all files
    ${FilesList}     Set Variable      SupportLib/FilesList.txt

    #Copy files
    put files in dir  ${MACHINE}   ${MyServerDirLocal}  ${MyServerDir}
    
    #Close Connection
    close connection    ${MACHINE}
    #**************************************************************************************************************************************************************************

End To End Live Trafic Test
    ###For this test to work you need the client already set up with wget
    #**************************************************************************************************************************************************************************
    Test Name    ${TEST NAME}
    #**************************************************************************************************************************************************************************
    # # start up server Machine
    ${MACHINE}    Set Variable    server
    start connection    ${MACHINE}
    #Define MyServerDir
    #MyServerDir should be the host of all files on server
    #MyServerDir should be the local_root of vsftpd on server
    ${MyServerDir}   Set Variable    /root/files/Files
    #get user name of server
    ${Server_User}  Get Server User  ${MACHINE}
    #get user name of server
    ${Server_Password}  Get Server Password  ${MACHINE}
    #get user name of server
    ${Server_Address}  Get Server Addr  ${MACHINE}
    #set port
    ${port}    Set Variable    80
    #start python Server
    start python server  ${MACHINE}  ${MyServerDir}   ${port}
    
    ##Now prepare the client
    SetServerInfo   ${Server_User}   ${Server_Password}   ${Server_Address}
    
    #set MyFiles List textfile
    ${FilesList}     Set Variable      SupportLib/FilesList.txt
    #set Files List 
    @{Files}     SetFilesListClient    ${FilesList}
    #@{Files}    Set Variable     BENIGN_FILES/Benign_DOC.doc  MALICIOUS_FILES/Malicious_DOC.doc
    
    #set the client machine variable
    ${MACHINE2}    Set Variable    client

    #Setting up the methods
    @{METHODS}  Set Variable  HTTP_GET  HTTP_POST  HTTP_PUT  FTP_GET  FTP_PUT  FTP_APPEND

    #Setting up the HTTP Verisions:
    @{HTTPVERS}  Set Variable  1.0  1.1

    #generate test case plan
    @{MyTests}  Generate Test Cases  ${Files}   ${METHODS}   ${HTTPVERS}  
    
    #now run curl test for each test case
    :FOR  ${TEST}  IN  @{MyTests}
    \  RunTest   ${MACHINE2}   ${MACHINE}  ${TEST} 

    #kill python server
    stop python server  ${MACHINE}

    
    
    
