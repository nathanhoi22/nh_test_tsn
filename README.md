# README #
This Repository is built for testing network traffic in regards to TSN 
### What is this repository for? ###

1. Generating real live network traffic using the following methods:
HTTP_PUT
HTTP_GET
HTTP_POST
FTP_GET
FTP_PUT
FTP_APPEND

2. Generates PCAP files (run once that was already run)

3. Contains PCAP files in SupportLib/mycaps/

4. Contains scripts for Modifying pcap files

5. Contains scripts for replaying tcp traffic with tcpreplay

6. Contains Robot Framework txt scripts for testing

####TO DO:####

1. Generating replay traffic from pcap files with open vswitch commands

2. Monitoring of tsn controller and analysis components

3. pass/fail scenarios for benign and malicious file transfer

4. pass/fail scenarios for http with bad links

# NETWORK SETUP #

###**Remote Access controller**###
 
* Scripts are designed to run on one machine that can access other machines remotely through ssh command.  All scripts will be placed on this machine
* minimal setup requirements and a unified source for maintenance of test results
*need python modules paramiko and scp (pip install .)

###Remote controlled machines###

* machines to be controlled by the Remote Access controller:
        client machines (having curl,wget to act as file transfer client)
        server machines (having twisted, vsftpd to act as file transfer server)
        tsn controller 


# Running the Robot Scripts #

###**Understanding the components**###

1. main robot script is runRobot.sh, which runs all tests needed.  modify as nessessary.  this puts the output diretory into ./MainResults

2.  index.html points to ./MainResults if you are running httpd with this directory you will be able to see the results from anywhere (given that you have internet access)

3. configuration xml (MyClients.xml) needs to be modified according to particular tests.  MyClients.xml is an xml file that lists all the clients that the Remote Access Control will use.  format is simple with the remote client name for reference with given ip, username, password

###**Running the Test**###
once the runRobot.sh and MyClients.xml are configured according the the tests to run, just run runRobot.sh to start the test suite


# running the Live Network Generator  #

Computers needed => 3 (Remote Access Controller, Client, ftp/http server)

###**Modules needed on Remote Access Controller**###
1. LiveTrafficGenerator.py

2. FilesList.py

3. SSHController.py

4. SSHAuto.py

5. MyLogger.py

to be safe, just run keep all the modules in the SupportLib, 

##**Setup**##

###Step 1 **Modify LiveTrafficGenerator.py**:###

*Edit the following lines for specifications of your FTP/HTTP server machine to host files from
...
        MyFTPUser = 'root'
        MyFTPPass = 'password' 
        MyServer = '192.168.1.4'
        MyServerDir = '/root/files/Files'
...

*Edit the following lines for specification for you client machine to get/put files to/from server
...
        PutFilesDir = '/root/LOCALDIR' 
 ...


###Step2 **Start twisted server**###

verify that you have files in MyServerDir(specified in step 1)
In this directory you will have two python files used for different purposes

to start a server to serve as Upload for http PUT/POST method at port 80 use SimpleHTTPServerWithUploadWithPut.py

...

        python2.7 SimpleHTTPServerWithUploadWithPut.py 80

...

to start a server to serve as a GZIP encoder for http GET methon at port 80 use TwistedCompression.py

...

         python2.7 TwistedCompression.py -p 80

...

###Step3 **configure vsfptd server**###

ftp server needs to serve in MyServerDir(specified in step 1)
configure /etc/vsftpd/vsftpd.conf 

...
        
        local_enable=YES
        write_enable=YES
        local_root= /root/files/Files
        setsebool -P allow_ftpd_full_access 1
        sevice vsftpd restart

...

###Step 4 **Run script**###

run script on Remote Access Controller

syntax for generator is as such

python LiveTrafficGenerator.py -m <method> -p <path> -c <client> -p <server>

valid methods(-m):
HTTP_GET, HTTP_PUT, HTTP_POST, FTP_GET, FTP_PUT, FTP_APPEND

path(-p):
the path is a path AFTER the serve directory. for example if MyServerDir is /root/files/Files and path is set as such -p myfile.exe.  the file /root/files/Files/myfile.exe must exist on server

client(-c)
the client name that will appear on the MyClient.xml file for ssh clients
<client>
        <ip>192.168.1.5</ip>
	<username>root</username>
	<password>password</password>
</client>
in this case use -c 'client'

server(-s)
the server name that will appear on the MyClient.xml file for ssh clients
<blablabla>
        <ip>192.168.1.4</ip>
	<username>root</username>
	<password>password</password>
</blablabla>
in this case use -s 'blablabla'

####Examples:####
use http get to grab file from server
...

       cd nh_tsn_test/SupportLib
       python LiveTrafficGenerator.py -m 'HTTP_GET' -p 'MyFile' -c 'client', -s 'blablabla'

...

Use HTTP PUT to upload a file and use different HTTP version 

...

        python LiveTrafficGenerator.py -m 'HTTP_PUT' -p 'MyLocalFile' -c 'client', -s 'blablabla' -v '1.0'

...

to run using a different encoding/compression use -C option to get a file from the server

...

        python LiveTrafficGenerator.py -m 'HTTP_GET' -p 'MyFile' -c 'client', -s 'blablabla' -C 'gzip'

...

 
####Advanced alternative steps and automation ** Modify FilesList.py ***####

FilesList.py is a file containing an array of file names called TestFiles.
you may verify or modify the files in this list to match all files that the server is serving in MyServerDir (specified in step 1)

you can then write a python or shell script to loop this function
or you can modify the LiveTrafficGenerator.py module to use the runCurl method

...

        Methods = ['HTTP_GET',  'HTTP_PUT', 'HTTP_POST', 'FTP_GET', 'FTP_PUT', 'FTP_APPEND']
        Clients = ['client', 'client2']
        Servers= ['server', 'server2']
        HTTPVERSIONS = ['1.0','1.1','2.0']
        Compressions = [None, 'gzip']
          for File in TestFiles:
            for method in Methods:
              for client in Clients:
                 for server in Servers:
                    for HTTPVERSION in HTTPVERSIONS:
                       for Compression in Compressions:
                           LiveTrafficGenerator.runCurl(filePath = File,
                                                            Method =method,
                                                            Client = client,
                                                            Server = server,
                                                            HTTPVER = HTTPVERSION,
                                                            Encoding = None,
                                                            )

...