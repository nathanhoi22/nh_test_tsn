from TSE_Controller import *
from time import sleep

class DomainRestartTest(): #Frodo-story-1021
    def __init__ (self,
                  ):
        self.cont = 0

        ##########################################
        ##MAKE SURE VERIFY COMMANDS ARE RUN LAST##
        ##########################################
    def start_connection(self,
                        Machine):
        self.machine = Machine
        self.TSEControl = TSE_Controller(self.machine)
        
    def Restart_Domain(self,
                       Test = 'Redis', #'date'
                       WaitMinutes = 10, #will fail test if longer than this
                       ):

        assert WaitMinutes > 5, 'invalid wait time, set wait time to at least 5 minutes'
        #set redis command if specified, otherwise manually set date to saturday just before switch to sunday

        #set redis command 
        if Test.lower() == 'redis':
            MyCommand = 'redis-cli PUBLISH  runtime:config:notification update.domains'        
            
        #set date command
        if Test.lower() == 'date':
            MyCommand = 'date --set="2014:11:01 23:59:59'

        #get VM Status with verification that all machines are running
        i = 0
        while i < WaitMinutes:
            readyKeys = []
            Continue = 1
            Log ('waiting for all machines to be running')
            self.OriginMachines = self.TSEControl.GetVMStatus()
            for KEY in self.TSEControl.VMList.keys():
                if KEY not in readyKeys:
                    if Continue == 1:
                        if self.TSEControl.VMList[KEY]['Ready'] == True:
                            Continue = 1
                            readyKeys.append(KEY)
                        else:
                            Log('key %s not done' %KEY)
                            Continue = 0
                            sleep(60)
                            i+= 0
            if Continue == 1:
                break
            if i > WaitMinutes:
                break
        assert Continue == 1, 'not all machines are running and it has been %d minutes' %WaitMinutes
    
        

        #get start time
        i,ST,e = self.TSEControl.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand('date')
        StartTime = ST[0].encode()
        timeset = r'\d+:\d+:\d+'
        StartTime = re.findall(timeset, StartTime)[0]
        ##Now run the command on the TSE machine
        self.TSEControl.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(MyCommand)

        #sleep for waitminutes
        Log('waiting %i minutes' %(60 * int(WaitMinutes)))
        sleep(60 * int(WaitMinutes))

        self.NewMachines = self.TSEControl.GetVMStatus()
        FinishTime = ''
        TimeOut = 0
        for KEY in self.NewMachines.keys():
            if self.NewMachines[KEY]['Ready'] != True:
                Log('%s machine not ready' %KEY)
                TimeOut = 1
            else:
                Log('setting finish time')
                Ft = self.NewMachines[KEY]['ReadyTime']
                if Ft > FinishTime:
                    Log('setting finish time')
                    FinishTime = Ft
                    
        #convert StartTime to seconds
        ##remove code for now
        assert TimeOut == 0, 'restarting domains too longer than %i minutes' %int(WaitMinutes)
            
        Log ('start time = %s, finish time = %s' %(StartTime, FinishTime))

    def Verify_All_Machines_Reset(self,
                                  ):
        Log('Verifying all machines have reset with new ids')
        #verify same number of vms
        assert len(self.OriginMachines.keys()) == len(self.NewMachines.keys()), 'there should be the same number of machines in the end as there was in the beginning old = %d, new = %d' %(len(self.OriginMachines.keys()),len(self.NewMachines.keys()))
        #search through keys
        for KeyO in self.OriginMachines:
            for KeyN in self.NewMachines:
                if KeyO == KeyN:
                    Log('keys are the same. check id number')
                    assert self.OriginMachines[KeyO]['id'] != self.NewMachines[KeyN]['id'], 'Ids are the same, should have restarterded all machines'
                
                assert self.OriginMachines[KeyO]['id'] != self.NewMachines[KeyN]['id'], 'Ids  of %s and %s are the same, should have restarterded all machines at id = %s' %(KeyO, KeyN, self.NewMachines[KeyN]['id'])
        Log('all vms have new ids')
    def Verify_All_MD5(self,
                       ):
        Log('Verifying all qcows are new with new md5s')
        #verify same number of vms
        assert len(self.OriginMachines.keys()) == len(self.NewMachines.keys()), 'there should be the same number of machines in the end as there was in the beginning old = %d, new = %d' %(len(self.OriginMachines.keys()),len(self.NewMachines.keys()))
        #search through keys
        for KeyO in self.OriginMachines:
            for KeyN in self.NewMachines:
                if KeyO == KeyN:
                    Log('keys are the same. check md5')
                    assert self.OriginMachines[KeyO]['md5'] != self.NewMachines[KeyN]['md5'], 'md5 values are the same for %s and %s' %(KeyO,KeyN)
                assert self.OriginMachines[KeyO]['md5'] != self.NewMachines[KeyN]['md5'], 'md5 values are the same for %s and %s' %(KeyO,KeyN)
        Log('all qcow have new md5s')
    def Verify_QCOW_number(self,
                           ):
        Log('Verifying the number of qcows is equal to the number of vms')
                           
        #get # of VMS
        MyCommand = 'virsh -q list --all | wc -l'
        i,o,e = self.TSEControl.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(MyCommand)
        VMNumber = int(o[0].encode())

        #get # of qcow files
        MyCommand = 'ls /opt/local/frodo/sandbox/active/ | wc -l'
        i,o,e = self.TSEControl.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(MyCommand)
        QCOWNumber = int(o[0].encode())
        assert VMNumber == QCOWNumber, 'number of vms=%s, number of qcow=%s' %(VMNumber,QCOWNumber)
        Log('number of qcows is the same as the number of vms')
#if __name__ == "__main__":
def TestThisScript():
    Log('Starting DomainsRestart Test', mode = 'w')
    
    self = DomainRestartTest()
    self.start_connection('legolas')
    
    #do this 20 times
    for i in range(20):
        self.Restart_Domain()
        self.Verify_All_Machines_Reset()
        #self.Verify_All_MD5()
        #self.Verify_QCOW_number()
    
    #return self
    

