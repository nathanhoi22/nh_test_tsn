#!/usr/bin/env python

from MyThread import *
from time import asctime
import threading
import os
import sys, getopt
from subprocess import Popen, PIPE


MaliciousFile = "Malicious_DOCX.docx"
BenignFile = "Benign_DOCX.docx"
MTA = 'root@test-frodo.localdomain'
MysubjectFile = 'test.mail'
Defaulttimes = 1
LocalFiles = os.listdir(os.getcwd())
Files = []  #files to send if not OneFile
MaliciousFiles = []
BenignFiles = []
for File in LocalFiles:
    if 'Malicious' in File:
        MaliciousFiles.append(File)
    if 'Benign' in File:
        BenignFiles.append(File)   
    Files.append(file)



def RunMutt(Type, #for single file, 1 for all multi files, 
            FileType, # #benign, malicious, both
            MalNumber, #number of malicios tries for type 2
            BenignNumber, #number of benign tries for type 2
            ):  #locally for now
    print 'runing with Type=%i, Filetype=%s,MalNumber=%i, BenignNumber=%i' %(Type, FileType, MalNumber, BenignNumber)
    #set files list
    MyFilesList = []
    if Type == 0:
        if FileType == 'malicious':
            MyFilesList.append(MaliciousFile)
        if FileType == 'benign':
            MyFilesList.append(BenignFile)
        if FileType == 'Both':
            MyFilesList.append(MaliciousFile)
            MyFilesList.append(BenignFile)
    if Type == 1:
        if FileType == 'malicious':
            MyFilesList += MaliciousFiles
        if FileType == 'benign':
            MyFilesList += BenignFiles
        if FileType == 'both':
            MyFilesList += BenignFiles
            MyFilesList += MaliciousFiles
    if Type == 2:
        for i in range(MalNumber):
            MyFilesList.append(MaliciousFile)
        for i in range(BenignNumber):
            MyFilesList.append(BenignFile)
    print 'MyFilesList', MyFilesList
    #now run the command for each specified file
        
    for File in MyFilesList:
        print 'check', File
        if os.path.exists(File) == False:
            return 'malicious file does not exist'
            print File, 'Does not exist'
        Subject = 'Send %s at %s' %(File,str(asctime()))
        print Subject
        MuttCommand = 'mutt -s "%s" %s -a %s < %s' %(Subject, MTA, File, MysubjectFile)
    
        #run command
        print MuttCommand
        Popen(MuttCommand, executable = '/bin/bash', stdout=PIPE, shell = True)
  

def ThreadMutt(argv):
    times = Defaulttimes
    try:
        opts, args = getopt.getopt(argv, 't:m:b:T',['times=', 'malicious=', 'benign=', 'TYPE='])
    except getopt.GetoptError:
        print 'failed to run error'
    Type = 0 #for single file, 1 for multi files, 2 for set number
    FileType = 'malicious' # #benign, both
    MalNumber = 0 #number of malicios tries for type 2
    BenignNumber= 0
    for opt, arg in opts:
        if opt == '-t':
            times = arg.strip('\n')                                          
        if opt == '-m':
            maltimes = arg.strip('\n')
            Type = 2
            MalNumber = int(maltimes)
        if opt == '-b':
            bentimes = arg.strip('\n')
            Type = 2
            BenignNumber = int(bentimes)
        if opt == '-T':
            TYPE= arg.strip('\n')
            if TYPE == 'b':
                FileType = 'benign'
            elif TYPE == 'B':
                Type = 1
                FileType = 'benign'
            elif TYPE == 'M':
                Type = 1
	    elif TYPE == 'A':
                Type = 1
                FileType = 'both'
            else:
		Type = TYPE                                    

    for i in range(int(times)):
        print 'running'
        ###thread
        Run = MyThread(myfunc = RunMutt, args = [Type, FileType, MalNumber, BenignNumber])
        Run.run()
        ###dont thread
        #RunMutt(Type, FileType, MalNumber, BenignNumber)
if __name__ == "__main__":
    ThreadMutt(sys.argv[1:])
   
