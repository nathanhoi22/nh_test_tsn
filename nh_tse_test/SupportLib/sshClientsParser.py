import os
import xml.dom.minidom
from xml.dom.minidom import parse

#this will also look for any environmental variables passed to it

class MyXMLParser():
    def __init__(self, Machine):
        self.xmlfile = None
        self.cont = 0
        self.XMLDOMTREE = None
        self.XMLdoc = None
	self.SSHClients = {}
	self.OpenXML()
	self.Machine = Machine
	self.GetInfo(self.Machine)
	
    def OpenXML(self):
        self.xmlfile = './MyClients.xml' 
        if os.path.exists(self.xmlfile) == False:
            print ('%s does not exist' %self.xmlfile)
            self.cont = 1
        if self.cont == 0:
            self.XMLDOMTREE = xml.dom.minidom.parse(self.xmlfile)
            self.XMLdoc = self.XMLDOMTREE.documentElement
    
    def GetInfo(self, Machine):
        ###First Verify in any environmental variables have been set
        ip = os.getenv("%s_IP" %Machine)
        name = os.getenv("%s_NAME" %Machine)
        password = os.getenv("%s_PW" %Machine)

        if ( ip == None ) or (name == None) or (password == None):
            self.Client = self.XMLdoc.getElementsByTagName(Machine)
            ip = self.Client[0].getElementsByTagName('ip')[0].childNodes[0].data.encode('utf-8')
            name = self.Client[0].getElementsByTagName('username')[0].childNodes[0].data.encode('utf-8')
            password = self.Client[0].getElementsByTagName('password')[0].childNodes[0].data.encode('utf-8')
            self.SSHClients[Machine] = [ip, name, password]
        else:
            self.SSHClients[Machine] = [ip, name, password]
  
    
