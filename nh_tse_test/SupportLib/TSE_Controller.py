#!/usr/bin/env python

from subprocess import Popen, PIPE
from time import asctime
import os
from MyLogger import *
from SSHController import *
import re

#TIMEZONE = 'EST'

class TSE_Controller():
    def __init__(self, tseMachine):
        self.cont = 0
        self.sshcontrol = SSHController()
        self.machine = tseMachine
        self.VMList = {}
        self.start_connection()
    def start_connection(self):
        self.sshcontrol.ConnectObjects[self.machine] = self.sshcontrol.connect_to_machine(machine = self.machine)
        assert self.sshcontrol.cont == 0, 'failed to connect to client machine'
    def ReadFile(self,
                 FILE = '/data/var/log/frodo/frodo-analysis0.0.log' #default read logs
                    ):
        self.LogLines = self.sshcontrol.ConnectObjects[self.machine].readfile(FILE)
    def GetListVM(self):
        self.VMList = {}
        listcommand = ['virsh', '--connect', 'qemu:///system','list', '--all']
        if self.machine.lower() == 'local':
            Verify = Popen(listcommand, stdout=PIPE)
            ReadLines = Verify.stdout.readlines()
        else:
            GetList = ''
            for command in listcommand:
                GetList += command + ' '
            Input,Output, Error = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(GetList)
            ReadLines = Output
                       
        for line in ReadLines:
            try:
                value = []
                for split in line.decode().split('  '):
                    if split == '':
                        pass
                    else:
                        value.append(split)
                Id = value[0].encode()
                name = value[1].strip(' ')
                state = value[2].strip('\n').encode()
                if name.lower() == 'name':  #dont include this in list
                    pass
                else:
                    self.VMList[name.encode()] = {}
                    self.VMList[name.encode()]['id'] = Id
                    self.VMList[name.encode()]['state'] = state
                    Input, Date, Error = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand('date')
                    self.VMList[name.encode()]['LastCheckTime'] = Date[0]
            except IndexError:
                pass        
        self.retieveXMLinfo() #leave out for now
    def ControlVM(self,
                  StartStop,
                  Name):
        self.GetListVM()
        VMStartCommand = ['virsh', 'start', Name]
        VMStopCommand = ['virsh', 'shutdown', Name]
        if StartStop.lower() == 'start':
            if Name.lower() in str(self.VMList).lower():
                if self.machine.lower() == 'local':
                    MyStart = Popen(VMStartCommand,
                                    stdout = PIPE,
                                    )
                    ReadLines = MyStart.stdout.readlines()
                else:
                    Startvm = ''
                    for command in VMStartCommand:
                        Startvm += command + ' '
                    Input,Output, Error = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(Startvm)
                    ReadLines = Output
                for line in ReadLines:
                    print('attemting to log line: %s' %str(line))

        elif StartStop.lower()== 'stop':
            if Name.lower() in str(self.VMList).lower():
                if self.machine.lower() == 'local':
                    MyStop = Popen(VMStopCommand,
                                    stdout = PIPE,
                                    )
                    ReadLines = MyStop.stdout.readlines()
                else:
                    Stopvm = ''
                    for command in VMStopCommand:
                        Stopvm += command + ' '
                    Input,Output, Error = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(Stopvm)
                    ReadLines = Output
                for line in ReadLines:
                    print('attemting to log line: %s' %str(line))
            

    def StartAllVMS(self):
        self.GetListVM()
        for Name in self.VMList.keys():
            if self.VMList[Name]:
                if 'shut' in str(self.VMList[Name][1]).lower():
                    self.ControlVM('Start', Name)
    
    def StopAllVMS(self):
        self.GetListVM()
        for Name in self.VMList.keys():
            if self.VMList[Name]:
                if 'running' in str(self.VMList[Name][1]).lower():
                    self.ControlVM('Stop', Name)
                if 'paused' in str(self.VMList[Name][1]).lower():
                    self.ControlVM('Stop', Name)
                if 'idle' in str(self.VMList[Name][1]).lower():
                    self.ControlVM('Stop', Name)
       


    def retieveXMLinfo(self,):
        #for now just get UUIDs
        for Name in self.VMList.keys():
            if self.VMList[Name]:
                xml = ''
                MyCommand = 'virsh dumpxml %s' %Name
                if self.machine.lower() == 'local':
                    xml = Procs = Popen(MyCommand, executable = '/bin/bash', stdout=PIPE, shell = True).stdout.readlines()
                else:
                    Input,xml, Error = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(MyCommand)
                if xml == '':
                    print 'error getting xml'
                else:
                    uuid = r'<uuid>([^<]+)<'
                    for line in xml:
                        #print 'line:', line
                        #print 'search', re.findall(uuid, line)
                        if re.findall(uuid, line):
                            self.VMList[Name]['UUID'] = re.findall(uuid, line)[0]


        
    def GiveVMdir(self, Name, local, remote):
        self.GetListVM()
        GiveCommand = ['virt-copy-in', '-d',Name, local, remote]
        if Name.lower() in str(self.VMList).lower():
            if 'running' in str(self.VMList[Name][1]).lower():
                Log('cant transfer file to running machine, stop first', File = '%s/%s/message.log'%(os.getcwd(), Name))
            else:
                if self.machine.lower() == 'local':
                    Log('transfering %s into %s %s' %(local, Name, remote), File = '%s/%s/message.log'%(os.getcwd(), Name))
                    Transfer = Popen(GiveCommand,
                                     stdout = PIPE,
                                     )
                    ReadLines = Transfer.stdout.readlines()
                else:
                    Give = ''
                    for command in GiveCommand:
                        Give += command + ' '
                    Log('transfering %s into %s %s' %(local, Name, remote), File = '%s/%s/message.log'%(os.getcwd(), Name))
                    Input,Output, Error = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand(Give)
                    ReadLines = Output
                for line in Transfer.stdout.readlines():
                    Log(line, File = '%s/%s/message.log'%(os.getcwd(), Name))
    def GetVMStatus(self):
        self.GetListVM()
        MyLogfiles = ['frodo-analysis0.9.log',
                      'frodo-analysis0.8.log',
                      'frodo-analysis0.7.log',
                      'frodo-analysis0.6.log',
                      'frodo-analysis0.5.log',
                      'frodo-analysis0.4.log',
                      'frodo-analysis0.3.log',
                      'frodo-analysis0.2.log',
                      'frodo-analysis0.1.log',
                      'frodo-analysis0.0.log',
                      ]
        LogFile = []
        for File in MyLogfiles:
            try:
                self.ReadFile(FILE = '/data/var/log/frodo/%s' %File)
                LogFile += self.LogLines
            except IOError:
                pass #it is ok if the files does not exist
        self.LogLines = LogFile
        for Key in self.VMList.keys():
            self.VMList[Key]['Ready'] = []
            self.VMList[Key]['ReadyTime'] = ''
            self.VMList[Key]['Status'] = ''
            self.VMList[Key]['StatusLog'] = []
            self.VMList[Key]['md5'] = ''
            for i in range(len(self.LogLines)):
                mac = '52-54-00%s'%str(Key.split('52-54-00')[1])
                if mac in self.LogLines[i]:
                    if 'environment has been saved' in self.LogLines[i]:
                        self.VMList[Key]['Ready'] = True
                        timeset = r'\d+:\d+:\d+'
                        #timeset = r'(.*?) %s' %TIMEZONE
                        self.VMList[Key]['ReadyTime'] = (re.findall(timeset, self.LogLines[i-1])[0]).encode()
                        
                    if 'status' in self.LogLines[i]:
                        toSearch = r'to ([\w]+)'
                        colonSearch = r'status: ([\w]+)'
                        withSearch = r'with status ([\w]+)'
                        MySearches = toSearch, colonSearch ,withSearch
                        
                        try:
                            #remove present status
                            for status in MySearches:
                                Status = {}
                                #get status
                                #status = r'to ([\w]+)'
                                if (re.findall(status, self.LogLines[i])[0]).encode():
                                    self.VMList[Key]['Status'] = (re.findall(status, self.LogLines[i])[0]).encode()
                                #get time set for status
                                #timeset = r'(.*?) %s' %TIMEZONE
                                timeset = r'\d+:\d+:\d+'
                                self.VMList[Key]['StatusTime'] = (re.findall(timeset, self.LogLines[i-1])[0]).encode()
                                LogValue = [self.VMList[Key]['Status'] , self.VMList[Key]['StatusTime']]
                                self.VMList[Key]['StatusLog'].append(LogValue)

                                #get restart status
                                
                                Log ('%s: VM status of %s changed to %s' %(self.VMList[Key]['StatusTime'], Key, self.VMList[Key]['Status']))

        
                        except IndexError as error:
                            pass
                                #print error, self.LogLines[i]
            #now Get MD5 for each
            if 'SBX-W7' in Key:
                startString = 'vm-SBX-W7-NSP-'
            else:
                startString = 'vm-SBX-XP-SP2-'
            QCOW = startString + self.VMList[Key]['UUID'] + '.qcow2'
            i,O, E = self.sshcontrol.ConnectObjects[self.machine.encode()].sendcommand('md5sum /opt/local/frodo/sandbox/active/' + QCOW)
            md5 = O[0].split(' ')[0]
            self.VMList[Key]['md5'] = md5
        return self.VMList
def testController():
    self = TSE_Controller('legolas')
    self.GetVMStatus()
    return self



#commands
#//opt/local/frodo/3rdparty/bitdefender/bdc -upd  && redis-cli PUBLISH runtime:config:notification update.bitdefender.signatures
#redis-cli PUBLISH  runtime:config:notification update.domains
#on /etc/frodo/frodo-analysis.conf ...## =-danalysis.probabilistic.sandbox.template.maxNumOfInstance=0
#top-ten largest ===>  du -hsx * | sort -rh | head -10
