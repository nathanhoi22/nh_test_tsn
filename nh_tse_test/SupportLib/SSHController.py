#!usr/bin/env python

from SSHAuto import MySSHClient
from sshClientsParser import MyXMLParser
from MyLogger import Log
from MyThread import MyThread
import os
import tarfile
from subprocess import Popen, PIPE

class SSHController():
    def __init__(self):
        self.cont = 0
        self.MyFilesList = []
        self.ConnectObjects = {}
    def connect_to_machine(self,
                           machine = '', #field in xml file
                           port = 22):
        assert self.cont == 0, 'error occured, please see log'
        Log('starting %s machine' %machine)

        #connect
        self.parse = MyXMLParser(machine)
        server,user,password = self.parse.SSHClients[machine]
        try:
            #Log('connecting to %s machine at %s@%s:%s with pw %s' %(machine,user,server,port,password))
            Connect = MySSHClient(server,port,user,password)
            Log('connected to %s machine' %machine)
        except Exception as error:
            Log('failure connecting to %s machine at %s because %s' %(machine, server, error))
            self.cont = 1

        assert self.cont == 0, 'error occured, please see log'

        self.ConnectObjects[machine.encode()] = Connect
        #run validate machine
        self.VerifyMyMachine(machine)
        return Connect

    def install_package(self,
                        machine = '', #opject variable like self.ConnectObjects['client']
                        package = '', #must be string
                        ):
        nRet = 0
        nRet, MACHINE = self.VerifyMyMachine(machine)
        assert nRet == 0, "machine %s is not started as object" %MACHINE
        #Verify client has package
        Input,Output, Error = self.ConnectObjects[machine.encode()].sendcommand('rpm -qa | grep %s' %package)
        Exists = 0
        for pack in Output:
            if package in pack.lower():
                Exists = 1
                Log('this is the package %s' %pack)
                break
            else:
                Exists = 0


        if Exists == 0:
            if CommandValidator(self.ConnectObjects[machine],
                                'yum -y install %s' %package,
                                'Complete',
                                AltExpect = 'nothing to do',
                                ) != 0:
                Log('failed to install %s.  See logs' %package)
                nRet = 1
        else:
            Log('%s is installed' %package)
        assert nRet == 0, 'failed to install %s.  See logs' %package

    def stop_iptables (self,
                       machine = '',
                       ):
        nRet, MACHINE = self.VerifyMyMachine(machine)
        assert nRet == 0, "machine %s is not started as object" %MACHINE
        #Stop IPTABLES
        IptablesIsRunning = 1
        if CommandValidator(self.ConnectObjects[machine],
                            '/etc/init.d/iptables status',
                            'is not running',
                             ) == 0:
            IptablesIsRunning = 0
        if IptablesIsRunning == 1:
            if CommandValidator(self.ConnectObjects[machine],
                                '/etc/init.d/iptables stop',
                                'Flushing firewall rules',
                                AltExpect = 'unloading modules') != 0:
                Log('failed to stop iptables')
                nRet = 1
        assert nRet == 0, 'failed to stop iptables'
    def RunFileValidation(self,
                          File1 = [],  #format of[file, machine], can be [file,local]
                          File2 = [], #format of[file, machine]
                          ):
        nRet = 0

        assert type(File1) == tuple or type(File1) == list, 'File1 needs to be in the format of an array but is %s' %str(type(File1))
        assert type(File2) == tuple or type(File2) == list, 'File2 needs to be in the format of an array, but is %s'%str(type(File2))
        if File1[1] != 'local':
            nRet, MACHINE1 = self.VerifyMyMachine(File1[1])
            assert nRet == 0, "machine %s is not started as object" %MACHINE1
        if File2[1] != 'local':
            nRet, MACHINE2 = self.VerifyMyMachine(File2[1])
            assert nRet == 0, "machine %s is not started as object" %MACHINE2

        print 'File1 = %s' % File1[0]
        print 'File2= %s' %File2[0]

        FileIsDir = 0
        if os.path.splitext(File1[0])[1] == '' or os.path.splitext(File2[0])[1] == '':
            FileIsDir = 1
        if FileIsDir == 0:
            #get file 1
            if File1[1].lower() == 'local':
                MyCommand = ['md5sum']
                MyCommand.append(File1[0])
                CommandString = ''
                for Command in MyCommand:
                        CommandString += Command + ' '

                print 'running %s on %s' %(CommandString,File1[1])
                Output = Popen(MyCommand, stdout = PIPE)
                Output1 = []
                for line in Output.stdout.readlines():
                        Output1.append(line)
                        print line
                Error1 = Output1
            else:
                Input, Output1, Error1 = self.ConnectObjects[MACHINE1].sendcommand('md5sum %s' %File1[0])
            md51 = Output1[0].split(' ')[0]

            #get file 2
            if File2[1].lower() == 'local':
                MyCommand = ['md5sum']
                MyCommand.append(File2[0])
                CommandString = ''
                for Command in MyCommand:
                        CommandString += Command + ' '
                print 'running %s on %s' %(CommandString,File2[1])
                Output = Popen(MyCommand, stdout = PIPE)
                Output2 = []
                for line in Output.stdout.readlines():
                    Output2.append(line)
                    print line
                Error2 = Output2
            else:
                Input, Output2, Error2 = self.ConnectObjects[MACHINE2].sendcommand('md5sum %s' %File2[0])
            md52 = Output2[0].split(' ')[0]

            if md51 == md52:
                nRet = 0 #files are the same
                Log('success, file %s from %s = file %s from %s' %(File1[0], File1[1], File2[0], File2[0]))
            else:
                nRet = 1
                Log ('#######output1###########')
                for line in Output1:
                    Log('   %s' %line)
                for line in Error1:
                    Log('   %s' %line)
                Log ('#######output2###########')
                for line in Output2:
                    Log('   %s' %line)
                for line in Error2:
                    Log('   %s' %line)
        assert nRet == 0, 'md5sum check failed'
        return nRet


    def make_directory(self,
                 machine = '',
                 Dir = '', #must be string
                       ):
        assert Dir != '', 'Directory does not have value'
        nRet, MACHINE = self.VerifyMyMachine(machine)
        assert nRet == 0, "machine %s is not started as object" %MACHINE
        # Make dir files
        Input, Output, Error = self.ConnectObjects[machine].sendcommand('cd %s' %Dir)
        if len(Output) > 0: #call should not return any text
            dirExists = 0
        elif len(Error) > 0:
            dirExists = 0
        else:
            dirExists = 1

        if dirExists == 0 :
            Log('creating directory %s' %Dir)
            if CommandValidator(self.ConnectObjects[machine],
                                'mkdir %s' %Dir,
                                None) != 0:
                Log ('failed to make directory')
                nRet = 1
        assert nRet ==0, 'failed to make dir'

    def copy_file(self,
                  machine ='',
                  LocalFile = '', #must be full path
                  RemoteDir = '', #one dir location to place files (with multiple dir structure you will need to run this command several times)
                  ):
        nRet, MACHINE = self.VerifyMyMachine(machine)
        assert nRet == 0, "machine %s is not started as object" %MACHINE

        nRet = 0
        #copy even if file exists (no need for md5checking etc
        if nRet == 0:
            #verify files exist in pwd
            if os.path.exists(LocalFile):
                #copy files into dir
                self.ConnectObjects[machine].putscp(LocalFile, '%s/%s'%(RemoteDir,os.path.basename(LocalFile)))
            else:
                Log('%s could not be found locally' %LocalFile)
                nRet = 1
        assert nRet ==0, 'failed to copy file'


    def VerifyMyMachine(self, machine = ''):
        nRet = 0
        #self.ConnectObjects == ConnectObjects
        #debug list all machines
        print 'Listing ALL KEYS'
        i = 1
        for key in self.ConnectObjects.keys():
            print 'key %d= ' %i, key
            i+=1
        try:
            Log('checking if object %s'%machine)
            print 'objectis', self.ConnectObjects[machine]
        except KeyError:
            try:
                machine = machine.encode()
                Log('checking if machine %s object exists ' %machine)
                print 'objectis', self.ConnectObjects[machine]
                nRet = 0
            except KeyError:
                nRet = 1
        print 'matching', machine, self.ConnectObjects[machine]
        return nRet, machine
    def close_connection(self,
                         machine = ''):
        nRet, MACHINE = self.VerifyMyMachine(machine)
        assert nRet == 0, "machine %s is not started as object" %MACHINE
        self.ConnectObjects[MACHINE].close()

def CommandValidator(Connection, #self.ClientConnect, spaelf.ServerConnect, self.TsConnect
                     Command, #tell ssh client what to do
                     Expect, #string in string of output from command
                     AltExpect = None,
                     ):
    ssh_stdin, ssh_stdout, sshstderr = Connection.sendcommand(Command)
    #comment this out because wget returns as error but does not mean it failed
    #if len(sshstderr) != 0:
    #        Log('Command could not be validated because of ' + str(sshstderr))
    #        return -1
    if len(ssh_stdout) == 0 and Expect == None: #consider this as passed
        return 0
    else:
        Log('checking output results')
        for line in ssh_stdout:
            if Expect != None:
                if Expect.lower() in str(line).lower():
                    Log('command successfully exicuted')
                    return 0
            if AltExpect != None:
                if AltExpect:
                    if AltExpect.lower() in str(line).lower():
                        Log('command successfully exicuted')
                        return 0
        Log('checking error results')
        #Now check Error for the same thing (needed because some functions like wget send output to erorr if output not specified)
        for line in sshstderr:
            if Expect != None:
                if Expect.lower() in str(line).lower():
                    Log('command successfully exicuted')
                    return 0
            if AltExpect != None:
                if AltExpect.lower() in str(line).lower():
                    Log('command successfully exicuted')
                    return 0
    Log('Did not find expected string in output of command')
    return 1
